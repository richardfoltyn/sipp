#!/bin/sed -rnf

# FIXME: licensing

# Applies the following changes to SIPP do-files files from NBER:
#   1. Remove any commands except for label definitions
#   2. Move NBER copyright to the top
#   3. Remove all commets except for copyright notice
#   4. Change all multiline comments to use /* */ 

# Some files have copyright notices using ** as comments. In a first step,
# convert any block comment prefix with ** on every line into a block comment
# using /* */. Block comment is allowed to span blank lines.

:lbl1

# Convert all blank lines with some whitespace to true blank lines, then
# collapse multiple consecutive blank lines into one.
s/^[[:space:]]*$/\n/Mg
s/\n{3,}/\n\n/g

/[*]{2,}.*((\n[ \t]*[^*\n]{2,}[^\n]*)|(@@@@@))/ {
    # drop blank lines right before EOF
    s/[\n]+(@@@@@)$/\n\1/
    s/^[ \t]*[*]{2,}//Mg
    # create /* */ block comment
    s|^|/*\n| 
    s|([^\n]*)\'|*/\n\1|
    # delete marker
    s/@@@@@//
    blblc
}
/[*]{2,}.*/ {
    # use some cheap trick to identify EOF in the pattern above, will delete
    # pattern afterwards.
    $ s/$/\n@@@@@/
    $!N;
    blbl1 
}

# Now keep only the /* */ comment containing the copyright, discard all other
# potential /* */ comments.
:lblc
# print copyright notice right away, this should be first in output file
\|/\*.*copyright.*\*/|I { 
    # In some files, copyright notice has " ;" at every line end
    s/ ;$//Mg
    p; 
    blblend 
} 
# delete all other comments
s|/\*.*\*/||g
/\/\*/N
//blblc

:lbltest
# keep delimit pragmas
/#delimit/ { H; b }

# keep (potentially multiline) label definitions
# Store label definition in hold space so they can be printed after the
# copyright notice.
# Note that in some files (full panels) some data fiels are commented out using
# *# as this would exceed the maximum number of variables for some Stata
# versions.
:lbla

/^[[:space:]]*(\*#)?[[:space:]]*la(b|be|bel)?\b.*;[[:space:]]*$/ { 
    # uncomment commented-out data fields
    s/^[[:space:]]*\*#(.*)$/\1/Mg
    # in 1992 full panel, PP-INTV variable is called pp_int; rename for consistency with
    # other panels.
    s/\bpp_int([[:digit:]])/pp_intv\1/g
    s/\bpp_int\b/pp_intv/g

    # rename education-related variables so they correspond to names in DDF files
    s/\batt_sch?([0-9]+)/att_schl\1/
    s/\bgrd_cmp?([0-9]+)/grd_cmpl\1/
    s/\bed_leve?([0-9]+)/ed_level\1/
    s/\bhigrad([0-9]+)/higrade\1/
    s/\bgeo_st([0-9]+)/geo_ste\1/
    
    s/\benrl_m([0-9]+)/enrl_mth\1/

    s/\bwksper([0-9]+)/wkspermn\1/
    s/\bclswk([0-9]+)/clsswrk\1/
    s/\bhrrat([0-9]+)/hrlyrat\1/
    s/\bmthjbw([0-9]+)/mthjbwks\1/
    s/\bmthwop([0-9]+)/mthwopwk\1/

    # remove trailing _ in some variable names in full panel files, as most other
    # variables do not use _ to separate variables from waves.
    s/\b(age|ms|esr)_([0-9]+)/\1\2/

    H; b 
}
/^[[:space:]]*(\*#)?[[:space:]]*la(b|be|bel)?\b.*/N
//blbla

# move hold space to pattern space, print pattern space at the end of buffer
:lblend
$ { g; p }
