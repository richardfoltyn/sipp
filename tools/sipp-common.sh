#!/bin/bash

# Common functions for SIPP scripts
# Author: Richard Foltyn
# License: GPLv2 or later

# Prints error message and terminates program
die() {
    echo "$@"
    echo  -e "Aborting... \n"
    exit 1
}

# Conditionally echo arguments $2 to $# if $1 is a non-empty string
echo_cond() {
    OPT_VERBOSE=$1
    shift 1
    [ -n "$OPT_VERBOSE" ] && echo -e "$@"
}

# Assert that first arguments is an existing and writable directory, otherwise
# terminate and print error message.
assert_dir_writable() {
    [ -d "$1" -a -w "$1" ] || 
        die "Directory \"$1\" does not exist or is not writable."
}
