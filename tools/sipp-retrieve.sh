#!/bin/bash

# Author: Richard Foltyn
# FIXME: license

# Retrieves all raw data, Stata do-files and/or dictionary files from the SIPP
# from the NBER site at http://www.nber.org/data/sipp.html

# FIXME: Weight files regex also matches core files for some panels, so these
# will be downloaded as well.

. "$(dirname $0)/sipp-common.sh"

# Associative arrays used, which are only available in bash >= 4.0.0
if [ ${BASH_VERSINFO[0]} -lt 4 ]; then
    die "BASH version >= 4 required!"
fi

# save current IFS to restore it later
IFS_SAVE=$IFS

print_usage() {
    cat << EOF

Usage: $(basename $0) [OPTIONS] -b BASEDIR [PANELS]

Valid OPTIONS:
    -a      Fetch all data sets and file types for given panel(s)
    -b      Base directory to store downloaded files (required)
    
    -d      Data sets to fetch. Valid values are:
              core      Core wave files
              fp        Full panel (1990-1993 panels only)
              jid       Job ID revisions (1990-1993 panels only)
              tm        Topical module files
              wgt       Weight data (1996-2008 panels)
            
            Multiple values can be given as 
              -d value1,value2    or 
              -d value1 -d value2

    -f      File types to fetch. Valid values are:
              data      (Zipped) raw data files
              dct       Data dictionary files
              do        Stata do-files
              ddf       Data description files
        
            The syntax is the same as for -d.

    -q      Quiet mode
    -r      Replace existing files
    -v      Verbose mode (wget, unzip, etc. output)
    -x      Extract zipped data files, delete archives
    
Valid PANELS:
  $PANELS_DEFAULT
  If unspecified, all panels are retrieved.
EOF
}


# Adds type of files to fetch (raw data, do-files, dicts) to arrays
# Arguments: $1: subdirectory to store files; $2: type extension
add_filetype() {
    # make sure we add each type only ones, as getopts allows for
    # passing the same option multiple times.
    IFS=", "
    for ftype in $*; do
        case $ftype in
            dat*|raw)   WANT_EXT[$EXT_DATA]=1
                        ;;
            dict|dct)   WANT_EXT[$EXT_DICT]=1
                        ;;
            do)         WANT_EXT[$EXT_DO]=1
                        ;;
            ddf)        WANT_EXT[$EXT_DESC]=1
                        ;;
            *)          die "Unknown file type: $ftype"
                        ;;

        esac
    done
    IFS=$IFS_SAVE
}

add_data() {
    IFS=", "
    for dta in $*; do
        case $dta in
            c|core)         WANT_DATA[core]=1
                ;;
            t|tm|topic*)    WANT_DATA[tm]=1
                ;;
            fp|full*)       WANT_DATA[fp]=1
                ;;
            jid|job*)       WANT_DATA[jid]=1
                ;;
            wgt|weight*)    WANT_DATA[wgt]=1
                ;;
            *)  die "Unknown data type: $dta"
                ;;
        esac
    done
    IFS=$IFS_SAVE
}



# file extensions that have to be fetched from server
# (zip, do, dct)
declare -a EXT_TYPES=()
declare -A WANT_EXT=()

EXT_DATA="zip"
EXT_DO="do"
EXT_DICT="dct"
EXT_DESC="ddf"

# names of subdirectories for each extension
declare -A SUBDIRS=([$EXT_DATA]=raw [$EXT_DICT]=dict [$EXT_DO]=do \
    [$EXT_DESC]=ddf)

declare -A WANT_DATA=()
declare -a DATA_TYPES=()


# default panels to retrieve
PANELS_DEFAULT="1990 1991 1992 1993 1996 2001 2004 2008"
# base URL pointing to directory where SIPP stuff is stored on NBER server
BASEURL="http://www.nber.org"
INDEXURL="${BASEURL%/}/data/sipp.html"


if [ -z "$1" -o "$1" == "--help" ]; then
    print_usage
    exit
fi

# By default, operation is somewhat verbose (but not extra verbose as with -v)
OPT_VERBOSE=1

while getopts "ab:d:f:qrvx" OPTION
do
    case $OPTION in 
        a)  OPT_ALL=1
            ;;
        b)  BASEDIR=${OPTARG%/} # drop trailing slash if present
            ;;
        d)  add_data "$OPTARG" 
            ;;
        f)  add_filetype "$OPTARG"
            ;;
        q)  unset OPT_VERBOSE
            ;;
        r)  OPT_REPLACE=1
            ;;
        v)  OPT_EXTRA_VERBOSE=1
            ;;
        x)  OPT_EXTRACT=1
            ;;
        ?)  print_usage
            exit 1
            ;;
    esac
done

shift $(($OPTIND -1))

if [ -n "$OPT_ALL" ]; then
    EXT_TYPES=($EXT_DATA $EXT_DICT $EXT_DO $EXT_DESC)
    DATA_TYPES=(core tm jid fp wgt)
else
    EXT_TYPES=(${!WANT_EXT[@]})
    DATA_TYPES=(${!WANT_DATA[@]})
fi
    
# fetch only panels that were requested. If empty, fetch all
PANELS=${*-$PANELS_DEFAULT}

# make sure the base directory exists, if not, create it
[ -n "$BASEDIR" ] || die "No base directory provided!"

# make sure some data set and file types were chosen
[ ${#EXT_TYPES[@]} -gt 0 ] || die "At least one file type must be specified!"
[ ${#DATA_TYPES[@]} -gt 0 ] || die "At least one data set must be specified!"


# create required subdirectories
for ext in ${EXT_TYPES[@]}; do
    dir="$BASEDIR/${SUBDIRS[$ext]}"
    mkdir -p "$dir" >> /dev/null 2>&1 || die "Could not create directory \"$dir\"!"
    test -w "$dir" -a -x "$dir" || die "Insufficient permissions on directory \"$dir\"!"
done

# temporary directory to store HTML file with links to download files
if [ -z $TMP ]; then
    TMP=/tmp
fi
TMPFILE="$TMP/wget-$RANDOM.tmp"

WGET_OPTS=""
UNZIP_OPTS=""
if [ -z $OPT_EXTRA_VERBOSE ]; then
    WGET_OPTS="$WGET_OPTS -q"
    UNZIP_OPTS="$UNZIP_OPTS -qq"
fi

# get the HTML file that contains all links
wget $WGET_OPTS --output-document $TMPFILE "$INDEXURL" || 
    die "Failed to download index file"

# Make sure we do not match comments as there are lots of commented out files
# on the NBER page
# Strip comments using handy sed script from stackoverflow.com
sed -i -e :a -re 's/<!--.*-->//g;/<!--/N;//ba' -i "$TMPFILE"

declare -A REGEX_POST=([core]="w|l" [tm]="tm?" [jid]=jid [fp]=fp \
    [wgt]="lw|w")
declare -A REGEX_PRE=([core]="(sipp?)?l?" [tm]="(sipp?)?p?" [jid]="sipp?" \
    [fp]="sipp?" [wgt]="(sipp?)?(lgtwgt)?")

for ((i=0; i < ${#DATA_TYPES[@]}; i++)); do
    tmp_post[$i]=${REGEX_POST[${DATA_TYPES[$i]}]}
    tmp_pre[$i]=${REGEX_PRE[${DATA_TYPES[$i]}]}
done

IFS="|"
prefix="(${tmp_pre[*]})"
postfix="(${tmp_post[*]})"
IFS=$IFS_SAVE

echo_cond "$OPT_VERBOSE" "\nDownload summary:\n" "Panels: $PANELS\n"  \
    "Data sets: ${DATA_TYPES[@]}\n" "File types: ${EXT_TYPES[@]}\n"

for panel in $PANELS; do

    [ ${#panel} -ne 4 ] && die "Panels must be specified using 4-digit years!"

    for ext in ${EXT_TYPES[*]}; do
        dir=${SUBDIRS[$ext]}

        # some file have 4-digit years, others 2-digit.
        # Don't you love consistency?
        regex_pnl="(${panel:0:2})?${panel:2}"

        # 2008 panel has "pu" after 2-digit year in core and tm files
        match_str="HREF=\"/sipp/$panel/$prefix${regex_pnl}(pu)?$postfix[[:digit:]]{0,2}.$ext"

        urls=$(egrep "$match_str" "$TMPFILE" | sed 's/.*HREF="\([^"]*\).*/\1/')

        for url in $urls; do
            dst=$BASEDIR/$dir/$(basename $url)
            url="$BASEURL$url"

            # do not download again if either the ZIP file or an unzipped .dat
            # file is already present (.do and .dct files are never zipped)
            if [ ! -f $dst -a ! -f "${dst%$ext}dat" ] || [ -n "$OPT_REPLACE" ]; then
                echo_cond "$OPT_VERBOSE" "$url => $dst"
                wget $WGET_OPTS --output-document "$dst" "$url"
            else
                echo_cond "$OPT_VERBOSE" "File $dst exists. Skipping... "
            fi
            
            # unzip raw data files if required.
            # Unzip files even if they are present from a previous download but
            # were not extracted.
            if [ -f $dst -a $ext == "zip" -a -n "$OPT_EXTRACT" ]; then
                echo_cond "$OPT_VERBOSE" "Extracting $dst ..."
                unzip $UNZIP_OPTS -o "$dst" -d "$(dirname $dst)" 
                rm "$dst"
            fi                
        done
    done
done

rm $TMPFILE
