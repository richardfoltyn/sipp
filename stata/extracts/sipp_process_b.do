// File to process (somewhat) harmonized Set B data.
// Note that this file should be run from sipp_extract_b.ado.

// FIXME: CEPR copyright
// FIXME: licensing

version 12

// Argument: 4-digit panel year
args panel

notes: All variables are monthly except where noted.
notes: All variables come from the longitudinal panel except where noted.
notes: SIPP raw variable names used unless noted. See Crosswalk for more information on consistent names.

// recode all -1 to missing values
mvdecode _all, mv(-1)

rename sex female
notes female: SIPP: sex (90-93); esex (96+)
recode female 1=0
recode female 2=1
label define female 0 "Male" 1 "Female"
label values female female
label variable female "Female"

//------------------------------------------------------------------------------
// Age: sometimes age is not nondecreasing as it should be. This happens in
// panels from 2001 onwards, as for these panels longitudinal editing was
// dropped. 
// We correct this by ensuring that reported age is no smaller than age reported 
// in previous month.

sort uuid wave refmth
rename age age_orig
by uuid (wave refmth): sipp_nondecr age_orig, tag(flag_age_edit) generate(age)

label variable age_orig "(Unedited) age in reference month"
label variable age "Age in reference month"
label variable flag_age_edit "Flag indicating correction to age"
label define age_edit 0 "Age not edited" 1 "Age edited (nondecreasing in time)"
label values flag_age_edit age_edit
tabulate flag_age_edit

//------------------------------------------------------------------------------
// Race, ethnicity, citizenship

// Ethnic origin label definitions moved to external file
include "labels/labels_ethnicity.do"
label values ethnic lbl_ethnic

label variable ethnic "Ethnic Origin (90-93,96-01,04-08 differ)"
notes ethnic: SIPP: ethnicty (90-93); eorigin (96,01) ; not avail (04,08)

// RF: Note that the values for race are the same in the 04/08 panels
rename race race01
generate byte race04 = race01
label define race01 1 "White" 2 "Black" 3 "IndOrEsk" 4 "AsianPac"
label values race01 race01
label define erace04 1 "White alone" 2 "Black alone" 3 "Asian alone" 4 "Residual"
label values race04 erace04 
label variable race01 "SIPP race (unedited), 2001 panel and before"
label variable race04 "SIPP race (unedited), 2004 panel"

if `panel' < 2004 {
	replace race04 = .
}
else {
	replace race01 = .
}

// generate cepr race
generate byte race = .
if `panel' <= 1993 {
    replace race = 1 if race01 == 1 & (ethnic<14 | ethnic>20)
    replace race = 2 if race01 == 2 & (ethnic<14 | ethnic>20)
    replace race = 3 if (ethnic>=14 & ethnic<=20)
    replace race = 4 if race01>2 & (ethnic<14 | ethnic>20) & !missing(race01)
}
else if `panel' == 1996 | `panel' == 2001 {
    replace race = 1 if race01 == 1 & (ethnic<20 | ethnic>28)
    replace race = 2 if race01 == 2 & (ethnic<20 | ethnic>28)
    replace race = 3 if (ethnic>=20 & ethnic<=28)
    replace race = 4 if race01>2 & (ethnic<20 | ethnic>28) & !missing(race01)
}
else if `panel' == 2004 | `panel' == 2008 {
    replace race = race04
    recode race (3 = 4)
    replace race = 3 if ethnic == 1
} 

label define race_cepr 1 "White" 2 "Black" 3 "Hispanic" 4 "Other"
label values race race_cepr
label variable race "Race (W,B,H,O)"
notes race: 90-93 panels, generated from SIPP race and ethnicty
notes race: 96-08 panels, generated from SIPP erace and eorigin


if `panel' <= 1993 {
	gen citizen = 1 if tm8732 == 2 | tm8732 == 0 | tm8734 == 3
	replace citizen = 2 if tm8734 == 1
	replace citizen = 3 if tm8734 == 2
	notes citizen: SIPP: tm8732 & tm8734 (93)
        drop tm8732 tm8734
}
else if `panel' == 1996 {
	rename rcitiznt citizen
	notes citizen: SIPP: rcitiznt (96)
}
else if `panel' == 2001 {
	rename tcitiznt citizen
	notes citizen: SIPP: tcitiznt (01)
}
else if `panel' == 2004 | `panel' == 2008 {
	rename ecitizen citizen
	recode citizen (2 = 3)
	replace citizen = 2 if enatcit == 1
	notes citizen: SIPP: ecitizen and enatcit (04/08)
        drop enatcit
}

label define citizen 1 "Native" 2 "Naturalized" 3 "Not a naturalized citizen", modify
label values citizen citizen
label variable citizen "Citizenship status"
tab citizen
notes citizen: "citizen" is constant over the entire panel.
notes citizen: "citizen" is from T2.
	
tab disabled

recode disabled (2 = 0)
label variable disabled "Disability that limits work"
notes disabled: "disabled" is asked once per wave.
label define lmtwk 1 "disabled" 0 "not disabled", modify
label values disabled lmtwk
notes disabled: SIPP: disab (92,93); edisabl (96,01,04,08)
tab disabled

label variable ms "Marital Status"
notes ms: SIPP: ems (96,01,04,08)
tab ms, m

if `panel' >= 1996 {
        egen byte cohab = anymatch(erelat01-erelat30), values(2)
	tab wave cohab, m
	egen byte cohab_ = max(cohab), by(uuid)
	drop cohab
	rename cohab_ cohab
	drop erelat*
        label variable cohab "Cohabiting: Living in same HH as unmarried partner (96+)"
	notes cohab: SIPP: erelat01-erelat30
	notes cohab: "cohab" is constant over the entire panel.
	notes cohab: "cohab" is from T2.
}

// -------------------------------------------------------------------------------------------
// EDUCATION

label define ststat 1 "ft" 2 "pt" 3 "not enrolled"
label values ststat
label variable ststat "school status"
notes ststat: SIPP: att_sch (92,93); renroll (96,01,04,08)
notes ststat: "ststat" is asked once per wave.

if `panel' >= 1996 {
    recode student (2 = 0)
    label define student 0 "student this wave, but not this month" 1 "student this month" 
    label values student student
    *recode student .=0 /*this makes "student" consistent with earlier panels that code all others as 0*/
}
else if `panel' <= 1993 {
        label define student 0 "not student or not in univers/sample" 1 "student this month"
        label values student student
}
label variable student "student this month"
format %1.0f student
notes student: SIPP: enrl_mth (90-93); eenrlm (96,01,04,08)
tab student, missing


label define educ 1 "less than high school" 2 "high school grad" 3 "some college" ///
    4 "college degree" 5 "post-college"

// Years of schooling
// Note that the highest grade (HIGRADE or EEDUCATE) attained is not reported for children
// under the age of 15. 
// Ideally, we should be able to determine the years precisely computing (age - 6) if they 
// are enrolled in elementary school or high school.
// However, in all panels there are thousands of under-18 who 
// are not reported to be enrolled in school, so this is not really reliable.
// Alternatively we could just assume that everyone under 15 (?) must be enrolled in some school.

// Since I am going to eliminate all persons below the age of 16 or 18 from my sample I am not 
// really interested in fixing this at the moment.

// Some hints on how to map categorical eductional attainment to years of schooling is provided
// in Jaeger (1997): Reconciling the Old and New Census Education Questions.
// Since the CPS categories are almost identical to the SIPP categories for the 1996+ panels,
// I use his method to determine years of schooling.
// Note that Phd / Doctoral degree holders are assigned 18 years of schooling as in Jaeger, even
// if I have doubts whether someone can complete a PhD in this time.

// Concise format for debugging
capture format higrade %2.0f
capture format grd_cmpl %1.0f
format ststat %1.0f
capture format tm84* %1.0f
capture format eeducate %2.0f
capture format enlevel %2.0f

if `panel' <= 1993 {

    // Interpretation of higrade:
    // Values 21-24 are the first 4 years of college. Most likely someone who obtained a 
    // bachelor's degree will have a value of 24. Values 25-26 are postgraduate years.
    // No idea what happens if someone has more than 2 years of postgraduate studies, assume it's either
    // 25 or 26 in that case.
    generate byte educ = 1 if higrade < 12 | (higrade == 12 & grd_cmpl != 1)
    replace educ = 2 if higrade == 12 & grd_cmpl == 1
    replace educ = 3 if higrade>=21 & higrade<=24
    replace educ = 4 if higrade == 24 & grd_cmpl == 1
    replace educ = 5 if higrade>24 & !missing(higrade)
    notes educ: "SIPP: higrade & grd_cmpl (90-93)"

    // asume that the information in the topical module file is more accurate
    // (but it is valid only for wave 2)
    // highest grade 12 or less and no hs diploma or ged
    replace educ = 1 if tm8400 == 1 & tm8408 == 2
    // highest grade 12 or less and has hs diploma or ged
    replace educ = 2 if tm8400 == 1 & tm8408 == 1
    // highest grade 1 year of college and  ass., voc, or no degree
    replace educ = 3 if tm8416 == 1 & tm8422 >= 5 & tm8422 <= 7
    // highest grade 1 year of college and ba degree
    replace educ = 4 if tm8416 == 1 & tm8422 == 4
    // highest grade 1 year of college and adv. degree
    replace educ = 5 if tm8416 == 1 & tm8422 >= 1 & tm8422 <= 3 & tm8430 == 1

    // construct years of schooling
    generate educ_yr = max(0, min(age - 5, cond(grd_cmpl == 1, higrade, higrade - 1))) ///
        if higrade >= 1 & higrade <= 12
    replace educ_yr = cond(grd_cmpl == 1, higrade - 8, higrade - 9) if higrade >= 21 & higrade <= 26
    // Corrent that higrade cannot give the correct number of years for personst with more than a 
    // master's degree.
    // professional degree (law, medicine, etc.)
    replace educ_yr = 18 if tm8422 == 2
    // phd, doctorate
    replace educ_yr = 18 if tm8422 == 1

    recode ed_level (0 = .)

    drop tm84* higrade grd_cmpl

    // For those who report to be enrolled in some school we can use additional information
    // for elementary and highschool just compute years of education as age - 6.
    // For whatever reason, there are people up to 50 years old who report being enrolled in elementary school
    // therefore restrict age
    replace educ_yr = (age - 6) if missing(educ_yr) & (ststat == 1) & (ed_level == 1 | ed_level == 2) & ///
        age >= 6 & age <= 19

    // Never correct educ_yr downwards as someone might be enrolled for whatever reasons
    // after already receiving one degree.
    // For students enrolled in college: ed_level == 3 is first year in college, etc.
    replace educ_yr = 12 + (ed_level - 3) if (ststat == 1 | ststat == 2) & ///
        (ed_level >= 3 & ed_level <= 8) & (missing(educ_yr) | educ_yr < 12 + (ed_level - 3))
    // enrolled in vocational school, technical school or business school
    // Assume that the average person is in the second year
    replace educ_yr = 13 if (ststat == 1 | ststat == 2) & ///
        (ed_level == 9 | ed_level == 10 | ed_level == 11) & (missing(educ_yr) | educ_yr < 13)

    drop ed_level

}
else if `panel' >= 1996 {
    generate byte educ = 1 if eeducate<=38 & !missing(eeducate)
    replace educ = 2 if eeducate == 39
    replace educ = 3 if eeducate>=40 & eeducate<=43
    replace educ = 4 if eeducate == 44
    replace educ = 5 if eeducate>=45 & eeducate<=48
    notes educ: "educ" is asked once per wave.
    notes educ: SIPP: eeducate (96,01,04,08)

    // Years of schooling taken from Jaeger(1997), as mentioned above.
    gen educ_yr = 0 if eeducate == 31
    // 1st - 4th grade
    replace educ_yr = 2.5 if eeducate == 32
    // 5th - 6th grade
    replace educ_yr = 5.5 if eeducate == 33
    // 7th - 8th grade
    replace educ_yr = 7.5 if eeducate == 34
    // grades 9 to 12
    replace educ_yr = eeducate - 26 if eeducate >= 35 & eeducate <= 38
    // highschool diploma
    replace educ_yr = 12 if eeducate == 39
    // some college, no degree
    replace educ_yr = 13 if eeducate == 40
    // vocational, technical, business school, etc.
    replace educ_yr = 14 if eeducate == 41
    // associate degree
    replace educ_yr = 14 if eeducate == 43
    // Bachelors's degree
    replace educ_yr = 16 if eeducate == 44
    // Master's degree
    replace educ_yr = 18 if eeducate == 45
    // Professional degree
    replace educ_yr = 18 if eeducate == 46
    // PhD
    replace educ_yr = 18 if eeducate == 47

    // for those who report to be enrolled in some school we can use additional information
    // for elementary and highschool just compute years of education as age - 6
    replace educ_yr = (age - 6) if missing(educ_yr) & (ststat == 1) & (enlevel == 1 | enlevel == 2) & ///
        age >= 6 & age <= 19
    // Again, never correct educ_yr downwards, as someone might be enrolled for whatever reasons
    // after already receiving one degree
    // college 1st - 4th year
    replace educ_yr = 12 + (enlevel - 3) if (ststat == 1 | ststat == 2) & (enlevel >= 3 & enlevel <= 6) & ///
        (missing(educ_yr) | educ_yr < 12 + (enlevel-3))
    // first and second year graduate or professional school
    replace educ_yr = 16 + (enlevel - 7) if (ststat == 1 | ststat == 2) & (enlevel == 7 | enlevel == 8) & ///
        (missing(educ_yr) | educ_yr < 16 + (enlevel - 7))
    // vocational, technical, business school
    replace educ_yr = 13 if (missing(educ_yr) | educ_yr < 13) & (ststat == 1 | ststat == 2) & (enlevel == 9)
    // enrolled in college, not working towards degree
    // explictly exclude high-school students doind some courses in college, this would artificially
    // increase their years of schooling
    replace educ_yr = 13 if (missing(educ_yr) | educ_yr < 13) & (ststat == 1 | ststat == 2) &  ///
        (enlevel == 10) & eeducate > 38 ///

    drop enlevel eeducate
}


// general sanity check
replace educ_yr = (age - 5) if (age - educ_yr) < 5 & age >= 5

recode educ_yr (-100/0 = .)

// make sure that years of education are nondecreasing in time
sort uuid wave refmth
by uuid: sipp_nondecr educ_yr, replace

label variable educ_yr "Completed years of schooling (partially based on categorical data)"
label variable educ "Educational Attainment"
label values educ educ

tab educ_yr, nolabel missing

// -------------------------------------------------------------------------------------------
// Geography

tab metro
recode metro (2 = 0)
if `panel' >= 2004 {
    recode metro (3 = 0)
}
label define metro 1 "Metro" 0 "Residual", modify
label values metro metro
label variable metro "Metropolitan Residence"
notes metro: SIPP: hmetro (90,91,92,93); tmetro (96,01,04,08)
tab metro

if `panel' >= 2004 gen msa = .
label variable msa "Identifiable MSA/CMSA codes"
notes msa: SIPP: hmsa (90-93); tmsa (96,01); not avail. (04,08)


recode sipp_st (0 = .)
generate int state = sipp_st
* recode SIPP Code to CPS Code
tab state
if `panel' <= 2001 {
# delimit ;
    recode state (63=103)
    (1=63)
    (2=94)
    (4=86)
    (5=71)
    (6=93)
    (8=84)
    (16=82)
    (9=16)
    (35=85)
    (55=35)
    (54=55)
    (51=54)
    (10=51)
    (53=91)
    (11=53)
    (12=59)
    (13=58)
    (15=95)
    (33=12)
    (17=33)
    (32=88)
    (18=32)
    (23=11)
    (42=23)
    (19=42)
    (62=102)
    (47=62)
    (20=47)
    (61=101)
    (21=61)
    (22=72)
    (24=52)
    (25=14)
    (34=22)
    (26=34)
    (41=92)
    (27=41)
    (28=64)
    (29=43)
    (30=81)
    (45=57)
    (46=45)
    (31=46)
    (39=31)
    (36=21)
    (56=83)
    (37=56)
    (44=15)
    (38=44)
    (40=73)
    (48=74)
    (49=87)
    (50=13)
    ;
#delimit cr ;
}
else if `panel' >= 2004 {
* recode FIPS SIPP state code to CPS state codes
# delimit ;
recode state 
 (56=83) /* Wyoming */
 (55=35) /* Wisconsin */
 (54=55) /* West Virginia */
 (53=91) /* Washington */
 (51=54) /* Virginia */
 (50=13) /* Vermont */
 (49=87) /* Utah */
 (48=74) /* Texas */
 (47=62) /* Tennessee */
 (46=45) /* South Dakota */
 (45=57) /* South Carolina */
 (44=15) /* Rhode Island */
 (42=23) /* Pennsylvania */
 (41=92) /* Oregon */
 (40=73) /* Oklahoma */
 (39=31) /* Ohio */
 (38=44) /* North Dakota */
 (37=56) /* North Carolina */
 (36=21) /* New York */
 (35=85) /* New Mexico */
 (34=22) /* New Jersey */
 (33=12) /* New Hampshire */
 (32=88) /* Nevada */
 (31=46) /* Nebraska */
 (30=81) /* Montana */
 (29=43) /* Missouri */
 (28=64) /* Mississippi */
 (27=41) /* Minnesota */
 (26=34) /* Michigan */
 (25=14) /* Massachusetts */
 (24=52) /* Maryland */
 (23=11) /* Maine */
 (22=72) /* Louisiana */
 (21=61) /* Kentucky */
 (20=47) /* Kansas */
 (19=42) /* Iowa */
 (18=32) /* Indiana */
 (17=33) /* Illinois */
 (16=82) /* Idaho */
 (15=95) /* Hawaii */
 (13=58) /* Georgia */
 (12=59) /* Florida */
 (11=53) /* DC */
 (10=51) /* Delaware */
 (9=16) /* Connecticut */
 (8=84) /* Colorado */
 (6=93) /* California */
 (5=71) /* Arkansas */
 (4=86) /* Arizona */
 (2=94) /* Alaska */
 (1=63) /* Alabama */
;
#delimit cr ;
}

include "labels/labels_state_cps.do"

label values state lbl_state_cps
label variable state "CPS State Code (w/SIPP aggreg.)"
notes state: SIPP: geo_ste (90-93); tfipsst (96,01,04,08)
tab state
table state, c(mean sipp_st)

generate byte region = 1 if state == 11 | state == 12 | state == 13 | state == 14 | state == 15 | state == 16 
replace region = 2 if state == 21 | state == 22 | state == 23 
replace region = 3 if state == 31 | state == 32 | state == 33 | state == 34 | state == 35 
replace region = 4 if state == 41 | state == 42 | state == 43 | state == 44 | state == 45 | state == 46 | state == 47
replace region = 5 if state == 51 | state == 52 | state == 53 | state == 54 | state == 55 | state == 56 | state == 57 | state == 58 | state == 59
replace region = 6 if state == 61 | state == 62 | state == 63 | state == 64
replace region = 7 if state == 71 | state == 72 | state == 73 | state == 74
replace region = 8 if state == 81 | state == 82 | state == 83 | state == 84 | state == 85 | state == 86 | state == 87 | state == 88
replace region = 9 if state == 91 | state == 92 | state == 93 | state == 94 | state == 95
label variable region "Census Region (9)"

#delimit ;
label define lbl_region 
    1 "New England" 
    2 "Middle Atlantic" 
    3 "E. North Central" 
    4 "W. North Central" 
    5 "South Atlantic" 
    6 "E. South Central" 
    7 "W. South Central" 
    8 "Mountain" 
    9 "Pacific"
    ;
#delimit cr

label values region lbl_region
notes region: SIPP: geo_ste (90-93); tfipsst (96,01,04,08)

//------------------------------------------------------------------------------
// Formatting 

// ID variables
format %2.0f wave
format %1.0f refmth

// various indicator variables
format %1.0f female disabled ms metro 
capture format %1.0f cohab

// categorical variables
format %1.0f educ
format %2.0f race*
format %1.0f citizen
format %2.0f region
format %2.0f state sipp_st
format %04.0f msa
format %2.0f ethnic

format %2.0f age*
format %1.0f flag_age_edit
format %2.1f educ_yr

// drop annoying labels, these make things less clear
capture label drop refmth

// vim: set sts=4 sw=4 et:
