// Global paths for data, program, etc. directories


//------------------------------------------------------------------------------
// Version tag of Stata extraction routines (will be appended to data notes)
global extracts_version = "0.1"

// get home directory
local home: environment HOME
local dataroot = "/Data/SIPP"

// base directory of the git repository containing all SIPP code
local sipp_repo = "`home'/repos/sipp"

global log_dir = "`home'/stata/log"


// ------------------------------------------------------------
// PROGRAM DIRECTORIES
// Add ado-files in common/ to ADO path so we do not need to prefix them with
// directory.

adopath + "`sipp_repo'/stata/common"


// ------------------------------------------------------------
// NBER import directories

// directory where processed stata *.dta files were stored
global nber_out_dir = "`dataroot'/imported"
// for testing with a smaller subsample run misc/sipp_sample.do and point to
// this directory:
// global nber_out_dir = "`dataroot'/imported_sample"

// ------------------------------------------------------------
// Extract directories
global extracts_dir = "`dataroot'/extracts"

// For testing purposes: store extracts based on smaller subsample here
// global extracts_dir = "`dataroot'/extracts_sample"
