// Creates data set D of unifirm SIPP extracts.
// Set D contains employment and wage variables.

// Author: Richard Foltyn
// FIXME: cepr copyright
// FIXME: licensing

capture program drop sipp_extract_d
program define sipp_extract_d
    
    version 12
    syntax, Panel(integer) [SAVeas(string)]

    dbg_off sipp_assert_panel `panel'

    dbg_off clear

    if `panel' < 1996 {
        sipp_harmonize_d_pre96, panel(`panel')
    }
    else {
        sipp_harmonize_d_post96, panel(`panel')
    }

    do "sipp_process_d.do" `panel'

    sort uuid wave refmth
    order *, alphabetic
    compress

    label data "Set D: Employment and waves, Panel `panel' (Version ${extracts_version}, based on CEPR extracts 2.1.7)"
    notes _dta: "Created: `=c(current_date)', `=c(current_time)'" 

    if `"`saveas'"' != "" {
        save `"`saveas'"', replace
    }
    
    // some details for the log
    summarize

end // sipp_extract_d


//------------------------------------------------------------------------------
// Harmonizes 1996+ panels so they conform to common naming conventions and
// have comparable variable values. 
capture program drop sipp_harmonize_d_post96
program define sipp_harmonize_d_post96

    version 12
    syntax, Panel(integer)

    // Variables from core wave files
    // Varname      DDF name        Description
    // ----------------------------------------
    // apmsum{1,2}  APMSUM{1,2}     Allocation flag for TPMSUM{1,2}
    // apyrate{1,2} APYRATE{1,2}    Allocation flag for TPYRATE{1,2}
    // eclwrk{1,2}  ECLWRK{1,2}     Class of worker (private, federal govt., etc.)
    // eeno{1,2}    EENO{1,2}       Unique job number that remains the same across waves
    // ejbhrs{1,2}  EJBHRS{1,2}     Usual hours worked per week at this job
    // ejbind{1,2}  EJBIND{1,2}     Industry code
    // ejobcntr     EJOBCNTR        Number of jobs held in reference period
    // epayhr{1,2}  EPAYHR{1,2}     Paid by the hour?
    // ersend{1,2}  ERSEND{1,2}     Main reason stopped working for employer
    // estlemp{1,2} ESTLEMP{1,2}    Still working for this employer?
    // eunion{1,2}  EUNION{1,2}     Union member on this job?
    // ecntrc{1,J2}  ECNTRC{1,2}     Covered by union or employee association contract?
    // rmesr        RMESR           Employment status recode for month
    // rmhrswk      RMHRSWK         Usual hours worked per week recode in month 
    // rmwkwjb      RMWKWJB         Number of weeks with a job in month
    // rwkesr#      RWKESR#         Employment status recode for week #
    // rwksperm     RWKSPERM        Number of weeks in this month
    // tbmsum{1,2}  TBMSUM{1,2}     Total income received from business this month
    // tejdate{1,2} TEJDATE{1,2}    Ending date of job
    // tempall{1,2} TEMPALL{1,2}    Number of employees at all locations
    // tempsiz{1,2} TEMPSIZ{1,2}    Number of employees at location
    // tjbocc{1,2}  TJBOCC{1,2}     Occupational classification code
    // tmlmsum      TMLMSUM         Amount of income from moonlighting or extra jobs
    // tpmsum{1,2}  TPMSUM{1,2}     Gross earnings from job received in this month
    // tpyrate{1,2} TPYRATE{1,2}    Regular hourly pay rate 
    // tsjdate{1,2} TSJDATE{1,2}    Starting date of job

   
    // variables erace eorigin and rmhrswk are pulled in only for the topcoding
    // it is much more efficent to get them right here than to repear the entire procedure 
    // in the topcoding code
    local varlist uuid swave srefmon ejobcntr rmesr rmwkwjb rwksperm eclwrk* eunion* ecntrc*  ///
        ejbhrs* tpmsum* tpyrate* ejbind* tjbocc* tempall* tempsiz* eeno* estlemp* tsjdate*  ///
        tejdate* ersend* apmsum* apyrate* tbmsum* tmlmsum rwkesr* epayhr* ///
        erace eorigin rmhrswk asjdate* rmwksab

    // obtain canonical file name (with a wildcard to match all wave indices)
    // used to save relevant wave files in import routines.
    dbg_off sipp_gen_fname, panel(`panel') dataset("core") wildcard

    // find imported core files present on disk, sort alphabetically
    local fn_waves: dir "${nber_out_dir}" files "`r(fname)'"
    local fn_waves: list sort fn_waves

    foreach fn of local fn_waves {
        dbg_off quiet append using "${nber_out_dir}/`fn'", keep(`varlist') `opts'
        // import notes and labels only on first iteration
        local opts = "nolabel nonotes"
    }

    rename rmesr        esr
    rename rwksperm     wksper
    rename ejbhrs*      wkhours*
    rename tpyrate*     rate*
    rename tpmsum*      earn*
    rename ejbind*      ind*
    rename tjbocc*      occ*
    rename eclwrk*      class*
    rename eunion*      unionm*
    rename ecntrc*      unionc*
    rename eeno*        job*
    rename epayhr*      hrly*
    rename tbmsum*      selfinc*
    rename tmlmsum      mooninc
    rename ejobcntr     njobs

    rename (swave srefmon) (wave refmth)
 
end // sipp_harmonize_d_post96


//------------------------------------------------------------------------------
// Harmonizes pre-1996 panels so they conform to common naming conventions and
// have comparable variable values. 

/*
Rough guide to merging job-data for pre-1996 panels:

We need data from three sources: full panel files, core wave files, topical
modules (for job starting dates).

However, these data are structured in an imcompatible way:
    - Full panel files are longitudinally edited. Here JOB-ID1 always contains
      the job ID of the primary job (mostly the one with more hours worked).
      Consequently, job-specific variables that are conceptually constant within
      a wave change whenever there is a change in primary jobs.

      Example: For first 2 months jobid101, jobid102 contain value 1; in 3rd
      month the primary job changes to 2, then jobid103 and jobid104 contain
      value 2. Analogously, earnings in ern_amt101 and ern_amt102 refer to
      earnings of job ID 1, while ern_amt103 and ern_amt104 refer to earnings
      for job ID 2.

    - Conversely, in core wave files wavely variables are always constant over
      the 4 months that constitute a wave. Continuing with the above example, 
      the variable WS12002 contains job ID 1 for all 4 months of wave 1; WS22102
      contains job ID 2 for all 4 months too, even though job ID 2 is the primary
      (or possibly only) job starting in 3rd reference month.

The above structure leads to invalid data when the full panel file is reshaped
into months, merged with core wave files using uuid wave refmth, and later
reshaped into job-month format. Then for any wave where a change of primary job
ID occurs in longitudinal file, wavely job characteristics from wave files will
be matched with wrong job ID.

Correct procedure to obtain a consistent merged data set: 
    1. Use edited variables from longitudinal file whenever possible;
    2. Merge longitudinal data with wave data only for monthly variables (these
        are hard to identify since the DDFs for pre-1996 panels do not document
        the frequency of data).
    3. Reshape merged data into job-month format.
    4. Separately load wavely variables from core wave files, collapse to one
        observation per wave; reshape into job-month format.
    5. Merge data sets from (3) and (4) using uuid job wave refmth.

The code below implements steps (1) and (2); step (3) is implemented in
sipp_process_d.do, step (4) in sipp_set_d_wavely.do; (5) is performed in
sipp_process_d.do again.

*/
capture program drop sipp_harmonize_d_pre96
program define sipp_harmonize_d_pre96

    version 12
    syntax, Panel(integer) 
 
    // Variables from the longitudinal files
    // Varname          DDF name        Description
    // --------------------------------------------
    // esr##            ESR             Employment status recode 
    // wkspermn##       WKSPERMN        Number of weeks in each month of reference period
    // jobid{1,2}##     JOB-ID{1,2}     Employer ID
    // clsswrk{1,2}##   CLSSWRK{1,2}    Class of employee (private, fed. govt., etc.)
    // occ{1,2}_##      OCC{1,2}        Edited and imputed 3 digit occupation code
    // ind{1,2}_##      IND{1,2}        Edited and imputed 3 digit industry code
    // ern_amt{1,2}##   ERN-AMT{1,2}    Amount of earnings for job # this month
    // wshrs{1,2}##     WS-HRS{1,2}     Usual hours per week worked at job #
    // hrlyrat{1,2}##   HRLYRAT{1,2}    Hourly pay rate for job #
    // se_amt{1,2}##    SE-AMT{1,2}     Amount of income received each month from this business 
    // mthjbwks##       MTHJBWKS        Number of weeks with job or business in month

    // variables to fetch from full panel file
    local vars_pnl uuid occ* ind* esr* mthjbwks* wkspermn* jobid* clsswrk* ///
        ern_amt* wshrs* hrlyrat* se_amt*

    dbg_off sipp_gen_fname, panel(`panel') dataset("fp")
    use `vars_pnl' using "${nber_out_dir}/`r(fname)'", clear

    rename esr(##)          esr(#)
    rename mthjbwks(##)     rmwkwjb(#)
    rename wkspermn(##)     wksper(#)
    rename jobid?(##)       job?(#)
    rename clsswrk?(##)     class?(#)
    rename occ?_(##)        occ?(#)
    rename ind?_(##)        ind?(#)
    rename ern_amt?(##)     earn?(#)
    rename wshrs?(##)       wkhours?(#)
    rename hrlyrat?(##)     rate?(#)
    rename se_amt?(##)      selfinc?(#)

    tempvar idx_mth
    dbg_off reshape long job1 job2 occ1 occ2 ind1 ind2 class1 class2 esr rmwkwjb wksper ///
        earn1 earn2 rate1 rate2 wkhours1 wkhours2 selfinc1 selfinc2, ///
        i(uuid) j(`idx_mth')

    generate byte refmth = mod(`idx_mth' - 1, 4) + 1
    generate byte wave = ceil(`idx_mth'/4)

    // drop 10th wave from 1992 panel; this was never released as core wave
    // file, we therefore cannot merge it with core data.
    if `panel' == 1992 {
        drop if wave > 9
    }

    // Monthly Variables from core wave files
    // Varname          DDF name        Description
    // --------------------------------------------
    // njobs            NJOBS           Number of wage/salary jobs this month
    // wesr*            WESR[1-8]       Week # employment status
    // ws*calc          WS{1,2}CALC     Imputed or calculated earnings
    // empled           EMPLED          Working for an employer, self-employed or both?
    local vars_mth njobs wesr* ws*calc empled

    // obtain canonical file name (with a wildcard to match all wave indices)
    // used to save relevant wave files in import routines.
    dbg_off sipp_gen_fname, panel(`panel') dataset("core") wildcard
    
    // find imported core files present on disk, sort alphabetically
    local fn_waves: dir "${nber_out_dir}" files "`r(fname)'"

    foreach fn of local fn_waves {
        dbg_off merge 1:1 uuid wave refmth using "${nber_out_dir}/`fn'", ///
            keepusing(`vars_mth') assert(master match match_update) nogenerate `opts'
        // import notes and labels only on first iteration
        local opts = "nolabel nonotes update"
    }

    rename ws(#)calc    apmsum(#)
    rename wesr*        rwkesr*

    // keep only those obs. with pp_mis = 1 in Set A
    // get name of Set A file
    sipp_set_name, panel(`panel') setname("A")
    dbg_off merge 1:1 uuid wave refmth using "${extracts_dir}/`r(fname)'", ///
        keepusing(pp_mis) nogenerate

    drop if pp_mis != 1
    drop pp_mis

end // sipp_harmonize_d_pre96

// vim: sts=4 sw=4 et: 
