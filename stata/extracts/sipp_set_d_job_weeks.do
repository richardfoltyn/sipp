// Computes the number of weeks on a particular job for the reference month.


// Author: Richard Foltyn
// FIXME: Licensing

version 12

// Argument: 4-digit panel year
args panel


// categorical variable documenting how number of weeks on the job were obtained 
generate byte flag_wksjob = .

if `panel' <= 1993 {
    sipp_dates_to_weeks if !missing(job) & !missing(wejdate), generate(wksjob) sdate(wsjdate) edate(wejdate)

    replace flag_wksjob = 1 if !missing(wksjob)

    // if wsjdate/wejdate are missing, use at least sjdate from topical module
    // file to compute potentially shorter first month
    tempvar wksjob2
    sipp_dates_to_weeks if !missing(job) & missing(wejdate), generate(`wksjob2') sdate(sjdate)
    replace wksjob = `wksjob2' if !missing(`wksjob2')
    replace flag_wksjob = 2 if !missing(`wksjob2')

}
else if `panel' >= 1996 {
    sipp_dates_to_weeks if !missing(job), generate(wksjob) sdate(sjdate) edate(ejdate)
    replace flag_wksjob = 2 if !missing(wksjob)
}

// should have no non-sensical values
assert wksjob >= 0


label variable wksjob "Weeks on job in reference month"
label variable flag_wksjob "Imputation flag for wksjob"

notes wksjob: "If job start/end dates are missing or outside of reference month, it is assumed that job spell lasted for entire reference month."

format %2.1f wksjob 
format %1.0f flag_wksjob

#delimit ;
label define lbl_flag_wksjob 
    1   "Imputed from wave-specific job start/end dates, if present (90-93)"
    2   "Imputed from job start/end dates, if present (90-08)"
    ;
#delimit cr

label values flag_wksjob lbl_flag_wksjob 

tab wksjob, missing

// vim: sts=4 sw=4 et:
