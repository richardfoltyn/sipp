/*
Pulls in wavely variables from core wave files and reshapes them to job-wave
format. These data are then merge 1:m with job-month observations from
full panel and core wave files.
See the hints in sipp_extract_d.ado why this is necessary.

NOTE: In the process we also eliminate all persons from the data set who have
at least one uuid-job-wave observation only in the core wave file or the full
panel file, but not in both.
This assures that we have fewer inconsistent/missing/nonsensical records later
on.

*/

// Author: Richard Foltyn
// FIXME: licensing


version 12

// argument: 4-digit panel year
args panel

preserve

// obtain canonical file name (with a wildcard to match all wave indices)
// used to save relevant wave files in import routines.
dbg_off sipp_gen_fname, panel(`panel') dataset("core") wildcard

// find imported core files present on disk, sort alphabetically
local fn_waves: dir "${nber_out_dir}" files "`r(fname)'"

/*
Wavely variables from core wave files
Varname          DDF name        Description
--------------------------------------------
ws*02            WS###02         Employer ID number
ws*03            WS###03         Worked for employer in previous wave? (not present in wave 1)
ws*16            WS###16         Month in which person became employed by this employer IN THIS WAVE
ws*18            WS###18         Day on which person became employed (this wave)
ws*20            WS###20         Ending month (for this employer IN THIS WAVE)
ws*22            WS###22         Day on which empl. ended (this wave)
ws*24            WS###24         Main reason for stopping work
ws*26            WS###26         Paid by the hour on this job?
ws*44            WS###44         Member of union on this job?
ws*46            WS###46         Covered by union / employee association contract?
iws*28           IWS###28        Imputation flag for hourly pay rate (WS###28)
*/

local vars_wav ws*02 ws*03 ws*16 ws*18 ws*20 ws*22 ws*24 ws*26 ws*44 ws*46 ///
    iws*28

clear *

foreach fn of local fn_waves {
    dbg_off quietly append using "${nber_out_dir}/`fn'", ///
        keep(uuid wave `vars_wav') `opts'
    local opts = "nolabel nonotes"
}

rename ws(#)*02     job(#)
rename ws(#)*03     estlemp(#)
rename ws(#)*16     wsmonth(#)
rename ws(#)*18     wsday(#)
rename ws(#)*20     wemonth(#)
rename ws(#)*22     weday(#)
rename ws(#)*24     ersend(#)
rename ws(#)*26     hrly(#)
rename ws(#)*44     unionm(#)
rename ws(#)*46     unionc(#)
rename iws(#)*28    apyrate(#)

local vars_wav job? estlemp? wsmonth? wsday? wemonth? weday? ersend? hrly? ///
     unionm? unionc? apyrate?

sort uuid wave 

// FIXME: remove assertion in final version; checks that these are indeed
// wavely variables, as we have assumed.
foreach lvar of varlist `vars_wav' {
    by uuid wave: assert `lvar' == `lvar'[1], fast
}

collapse (first) `vars_wav', by(uuid wave)

// convert to job-wave format.
// Get rid of wildcards for long variable names
local vars_reshape: subinstr local vars_wav "?" "", all

tempvar jobno
reshape long `vars_reshape', i(uuid wave) j(`jobno')

// at this point we have not recoded data, missing jobs have ID 0
drop if job == 0

tab job, missing

// Apply longitudinal editing to estlemp which might have been invalidated by
// the job ID revision change. See comments in 
// stata/import/revisions/10-jobid-20040205.do
    
sort uuid job wave

// Original encoding in wave files:
//  0 - Not in universe; 1 - Yes; 2 - No;
// All job obs. in wave 1 should have estlemp = 0, while estlemp > 0 for all other
// waves.
tab estlemp, missing

generate byte estlemp_revised = 2
replace estlemp_revised = 0 if wave == 1
by uuid job (wave): replace estlemp_revised = 1 if _n > 1 & ///
    wave[_n-1] == (wave - 1) & job == job[_n-1]

generate byte flag_estlemp_chan = (estlemp != estlemp_revised)

tempfile fn_data_wav
save "`fn_data_wav'", replace

restore

merge m:1 uuid job wave using "`fn_data_wav'", noreport 
// explicitly tabulate to get relative frequencies of obs. that are only in
// using. These can be only due to errors in job ID.
tab _merge

// Flag all obs. which have conflicting job ID in full panel and core wave data
// sets and thus cannot be merged. We keep the observations from the full panel
// files for these cases, as it is not obvious which ones are the correct ones.
// We do not want to drop them outright as they might contain other relevant
// information.
generate flag_merge_fail = (!missing(job) & _merge != 3)
keep if _merge == 1 | _merge == 3
drop _merge

format %1.0f flag_merge_fail
tab flag_merge_fail, missing

// tabulate changes in estlemp for full job-month sample.
tab flag_estlemp_chan, missing

replace estlemp = estlemp_revised if flag_estlemp_chan == 1
drop flag_estlemp_chan estlemp_revised


// FIXME: remove in final version. Check that wavely variables do not vary
// within job-wave, as assumed.
sort uuid job wave
foreach lvar of varlist `vars_reshape' {
    by uuid job wave: assert `lvar' == `lvar'[1], fast
}

//------------------------------------------------------------------------------ 
// LABELS
label variable flag_merge_fail "Failed to merge job obs. from full panel and core wave files (90-93 panels only)"
notes flag_merge_fail: "Some wavely job IDs are present only in full panel or core wave files. We keep observations as they might be useful for non-employment-related data."  



// vim: sts=4 sw=4 et:
