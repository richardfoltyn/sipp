/*
Processing of wage / earnings data. If either earnings or hourly wage rate are missing
they are imputed from the non-missing variable using imputed monthly working
hours.

Treatment of topcoding is rudementary at the moment, so far earnings /
wage observations are only tagged as such.
TODO: As in the original CEPR code, we could replace topcoded hourly wages with
means above threshold from CPS, or assign random earnings drawn from Pareto
distribution to replace topcoded values.
This, however, is only interesting for purely cross-sectional analyses, so I
leave this for now.

*/

// FIXME: CEPR copyright
// FIXME: licensing

// Argument: 4-digit panel year
args panel

if `panel' >= 1996 {
	generate byte flag_earn_sipp = apmsum >= 1 & apmsum <= 4 if !missing(earn) & earn > 0
	generate byte flag_rate_sipp = apyrate >= 1 & apyrate <= 4 if !missing(rate) & rate > 0
}
else if `panel' <= 1993 {
	generate byte flag_earn_sipp = (apmsum == 1) if !missing(earn) & earn > 0
	generate byte flag_rate_sipp = (apyrate == 1) if !missing(rate) & rate > 0
}
drop apmsum apyrate


// we need the gender variable from set B
dbg_off sipp_set_name, panel(`panel') setname("b")
tempvar set_b_merge
merge m:1 uuid wave refmth using "${extracts_dir}/`r(fname)'", ///
    generate(`set_b_merge') keep(master match) keepusing(female) ///
    nolabel nonotes

tab `set_b_merge'

// tag potentially earnings/wage topcoded observations
sipp_tag_topcodes, panel(`panel') wagetag(flag_rate_tc) earntag(flag_earn_tc)

// vars. only needed for topcoding in some panels
capture drop erace eorigin rmhrswk 

generate hours_m = wkhours * wksjob

* Generate "CEPR-preferred" series
* Note that this cannot be done in one step as written; user must manually check numbers identified below.

// Replace top-coded wages w/ mean above wages >= $30 in CPS (top-code adjusted using log-normal)
// TODO: Add years beyond 2007
/*
generate rate_tc = rate

replace rate_tc = 36.23 if year == 1992 & flag_rate_tc == 1 & female == 1
replace rate_tc = 37.64 if year == 1993 & flag_rate_tc == 1 & female == 1
replace rate_tc = 48.82 if year == 1994 & flag_rate_tc == 1 & female == 1
replace rate_tc = 43.83 if year == 1995 & flag_rate_tc == 1 & female == 1
replace rate_tc = 39.66 if year == 1996 & flag_rate_tc == 1 & female == 1
replace rate_tc = 39.58 if year == 1997 & flag_rate_tc == 1 & female == 1
replace rate_tc = 39.78 if year == 1998 & flag_rate_tc == 1 & female == 1
replace rate_tc = 38.40 if year == 1999 & flag_rate_tc == 1 & female == 1
replace rate_tc = 39.21 if year == 2001 & flag_rate_tc == 1 & female == 1
replace rate_tc = 38.61 if year == 2002 & flag_rate_tc == 1 & female == 1
replace rate_tc = 42.24 if year == 2003 & flag_rate_tc == 1 & female == 1
replace rate_tc = 42.71 if year == 2004 & flag_rate_tc == 1 & female == 1
replace rate_tc = 45.94 if year == 2005 & flag_rate_tc == 1 & female == 1
replace rate_tc = 43.16 if year == 2006 & flag_rate_tc == 1 & female == 1
replace rate_tc = 44.01 if year == 2007 & flag_rate_tc == 1 & female == 1

replace rate_tc = 37.19 if year == 1992 & flag_rate_tc == 1 & female == 0
replace rate_tc = 38.54 if year == 1993 & flag_rate_tc == 1 & female == 0
replace rate_tc = 46.23 if year == 1994 & flag_rate_tc == 1 & female == 0
replace rate_tc = 41.27 if year == 1995 & flag_rate_tc == 1 & female == 0
replace rate_tc = 39.15 if year == 1996 & flag_rate_tc == 1 & female == 0
replace rate_tc = 38.99 if year == 1997 & flag_rate_tc == 1 & female == 0
replace rate_tc = 40.73 if year == 1998 & flag_rate_tc == 1 & female == 0
replace rate_tc = 40.47 if year == 1999 & flag_rate_tc == 1 & female == 0
replace rate_tc = 39.10 if year == 2001 & flag_rate_tc == 1 & female == 0
replace rate_tc = 38.98 if year == 2002 & flag_rate_tc == 1 & female == 0
replace rate_tc = 44.43 if year == 2003 & flag_rate_tc == 1 & female == 0
replace rate_tc = 48.32 if year == 2004 & flag_rate_tc == 1 & female == 0
replace rate_tc = 50.18 if year == 2005 & flag_rate_tc == 1 & female == 0
replace rate_tc = 45.70 if year == 2006 & flag_rate_tc == 1 & female == 0
replace rate_tc = 46.84 if year == 2007 & flag_rate_tc == 1 & female == 0
*/

/*
// Replace top-coded earnings w/ mean above top-code calculated by Census for 1996; for other panels
// take ratio of Census mean to Pareto 90th (avg. over 1996 - 99) and apply to other years.
if `panel' != 1996 {
	* Values added: 2/14/04:
	gen earntc = earn
	replace earntc = 1.56*earntc_9 if flag_earn_tc == 1 & female == 0
	replace earntc = 1.27*earntc_9 if flag_earn_tc == 1 & female == 1
}

*/

/*
// Generate CEPR earnm and wage
// FIXME: for now I drop topcoding

// if we have hours worked and hourly wages, compute missing monthly earnings
gen double earntc = earn
gen double earnm = earntc
recode earnm (0 = .)
gen flag_earn_calc = 1 if !missing(rate_tc) & !missing(hours_m) & hours_m > 0 & rate_tc > 0 & missing(earnm)
replace earnm = (rate_tc*hours_m) if flag_earn_calc == 1
replace flag_earn_calc = 0 if !missing(earnm) & flag_earn_calc != 1
// indicate that earnings were computed from topcoded wage
replace flag_earn_tc = 3 if flag_earn_calc == 1 & flag_rate_tc > 0 & !missing(flag_rate_tc)
summarize earnm, detail
tab flag_earn_calc

// If we have (topcoded) mounthly earnings but no hourly wages,
// compute these if hours are known.
// Note that it is sufficient to compute hourly wages from earntc and not earnm,
// because if earnm differs from earntc this is only due to earnm being computed above.
// But in this case we already have hourly wages!
gen double wage = rate_tc 
recode wage (0 = .)
gen flag_wage_calc = 1 if !missing(earntc) & !missing(hours_m) & earntc > 0 & hours_m > 0 & missing(wage)
replace wage = earntc/hours_m if flag_wage_calc == 1
replace flag_wage_calc = 0 if !missing(wage) & flag_wage_calc != 1
summarize wage, detail
// indicate that wage was computed from topcoded earnings
replace flag_rate_tc = 3 if flag_wage_calc == 1 & flag_earn_tc > 1 & !missing(flag_earn_tc)
tab flag_wage_calc
*/


// For missing earnings, compute earnings from hourly wages if these are present.
generate byte flag_earn = 0 if !missing(job)
replace flag_earn = 1 if !missing(rate) & !missing(hours_m) & ///
    hours_m > 0 & rate > 0 & missing(earn)
replace earn = (rate * hours_m) if flag_earn == 1
// indicate that earnings were computed from topcoded wage by correcting
// topcoding flag.
replace flag_earn_tc = 3 if flag_earn == 1 & flag_rate_tc > 0 & !missing(flag_rate_tc)

tab flag_earn, missing

// for missing hourly wage rate, compute it from earnings if present
generate byte flag_rate = 0 if !missing(job)
replace flag_rate = 1 if !missing(earn) & !missing(hours_m) & earn > 0 & hours_m > 0 & missing(rate)

replace rate = earn/hours_m if flag_rate == 1
// indicate that hourly rate was computed from topcoded earnings
replace flag_rate_tc = 3 if flag_rate == 1 & flag_earn_tc > 0 & !missing(flag_earn_tc)

tab flag_rate, missing


//------------------------------------------------------------------------------
// LABELS AND FORMATTING

format %4.1f rate
format %6.1f earn 
format %4.1f hours_m 

// label variable rate_tc "Top-coded wages with mean above wages >= $30 in CPS"
label variable rate "Regularly hourly pay rate"

label variable earn "Monthly earnings"
label variable hours_m "Hours per month at that job"

label variable flag_earn "Earnings imputed from hourly wage rate?"
label variable flag_rate "Hourly wage rate imputed from monthly earnings?"

label variable flag_earn_sipp "Earnings imputed (in public SIPP files)"
label variable flag_rate_sipp "Wages imputed (if public SIPP files)"

#delimit ;

label define lbl_flag_earn
    0   "Not imputed"
    1   "Imputed from hourly wage rate"
    ;
label define lbl_flag_rate
    0   "Not imputed"
    1   "Imputed from monthly earnings"
    ;
label define lbl_flag_earn_sipp
    0   "Not imputed"
    1   "Earnings imputed (in repoted SIPP data)"
    ;
label define lbl_flag_rate_sipp
    0   "Not imputed"
    1   "Wages imputed (in reported SIPP data)"
    ;
#delimit cr

label values flag_earn lbl_flag_earn
label values flag_rate lbl_flag_rate
label values flag_earn_sipp lbl_flag_earn_sipp
label values flag_rate_sipp lbl_flag_rate_sipp

if `panel' <= 1993 {
    notes rate: Top-code is (90-93): $30.00 per hour
    notes earn: Top-code is (90-93): $8,333 per month
}
else if `panel' >= 1996 {
    notes rate: Top-code is: $35.00 per hour (08); $28.50 per hour (04); $29.00 per hour (01); $30.00 per hour (96)
    notes earn: Top-code is (96,01): $12,500 per month
    notes earn: Top-code is (04,08): $16,666.5 per month
}

// vim: sts=4 sw=4 et:
