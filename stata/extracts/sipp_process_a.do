// File to process (somewhat) harmonized ID and weight data for Set A.
// Note: File should be run from sipp_extract_a.

// Author: Richard Foltyn
// FIXME: CEPR copyright
// FIXME: licensing

version 12

// Argument: 4-digit panel year
args panel

notes: All variables are monthly except where noted.
notes: All variables come from the longitudinal panel except where noted.
notes: SIPP raw variable names used unless noted. See Crosswalk for more information on consistent names.


// recode all -1 to missing values
mvdecode _all, mv(-1)

if `panel' >=  1996 {
    // encode status for children under 15 as 0 for all panels.
    recode eppintvw (5=0)
}

capture label drop eppintvw
#delimit ;
label define lbl_eppintvw
    0   "Children under 15 during reference period (not applicable)"
    1   "Interview (self)"
    2   "Interview (proxy)"
    3   "Noninterview: Type Z (1996+) / Type Z refusal (90-93)"
    4   "Noninterview: pseudo type Z. Left sample during reference period (1996+) / Type Z other (90-93)"
    5   "Noninterview: left before interview month (90-92)"
    ;
#delimit cr

label values eppintvw lbl_eppintvw
notes eppintvw: "Categorical values differ between 1990-1993 and 1996+ panels!"

summarize wpfinwgt, detail
summarize lgtpnlwgt, detail

notes shhadid: SIPP: hh_add (90-93)
notes gvarstr: SIPP: varstrat (90-93)
notes eppintvw: SIPP: pp_intv (90-93)
notes epppnum: SIPP: pp_pnum (90-93)
notes eentaid: SIPP: pp_entry (90-93)
notes wpfinwgt: SIPP: fnlwgt (90-93)
notes whfnwgt: SIPP: hgwt (90-93)
notes wffinwgt: SIPP: fgwt (90-93)
notes wsfinwgt: SIPP: sgwt (90-93)
notes ssuid: SIPP: suid (90-93)

label variable shhadid "Address ID. This field identifies the entry address"
label variable eppintv "Person's interview status for interview"     
label variable uuid "Universally unique ID (unique across panels)"
label variable wffinwgt "Family weight for the reference month"
label variable whfnwgt "Household weight for the reference month"
label variable wpfinwgt "Person weight for the reference month"
label variable wsfinwgt "Related subfamily weight for the reference month"

label variable lgtpnlwgt "Longitudinal panel weight for full panel"
notes lgtpnlwgt: pnlwgt (90-93), lgtpnlwt (1996), last lgtpnwtX (2001,2004), ///
    last lgtpnXwt (2008).

capture label variable pp_mis "Person's interview status for this month (90-93)"

// date of refenrece month in Stata's format. Comes in handy, so generate it here.
generate refdate = ym(year, month)
format refdate %tm

label variable refdate "Date of reference month in Stata's %td format"
label variable month "Calendar month of the reference month"

// format numeric variables for more concise displaying
format %2.0f wave
format %1.0f eppintvw refmth
format %4.0f panel year
// does not exist int 1996+ panels
capture format %1.0f pp_mis

// some imported labels are not helpful, drop them
capture label drop spanel
capture label drop srefmon

// vim: sts=4 sw=4 et: 
