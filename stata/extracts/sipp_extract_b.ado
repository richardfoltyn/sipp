// Creates data set B of uniform SIPP extracts.
// Set B contains demographic and eductational variables.

// Author: Richard Foltyn
// FIXME: cepr copyright
// FIXME: licensing

capture program drop sipp_extract_b
program define sipp_extract_b

    syntax, Panel(integer) [SAVeas(string)]

    dbg_off sipp_assert_panel `panel'

    dbg_off clear

    if `panel' < 1996 {
        sipp_harmonize_b_pre96, panel(`panel')
    }
    else {
        sipp_harmonize_b_post96, panel(`panel')
    }

    do "sipp_process_b.do" `panel'

    sort uuid wave refmth
    order *, alphabetic
    compress

    label data "Set B: Demographics/Education, Panel `panel' (Version ${extracts_version}, based on CEPR extracts 2.1.7)"
    notes _dta: "Created: `=c(current_date)', `=c(current_time)'" 

    if `"`saveas'"' != "" {
        save `"`saveas'"', replace
    }
    
    // some details for the log
    summarize

end // sipp_extract_b

//------------------------------------------------------------------------------
// Harmonizes 1996+ panels so they conform to common naming conventions and
// have comparable variable values. 
capture program drop sipp_harmonize_b_post96
program define sipp_harmonize_b_post96

    version 12
    syntax, Panel(integer)
    
    // import only these variables from core files
    local varlist uuid swave srefmon esex erace eorigin tage ems edisabl renroll eenrlm  ///
        eenlevel eeducate tmetro tfipsst

    // Citizenship status is the in core wave file for these panels, no need to merge the data from
    // the topical module files
    if `panel' >= 2004 {
        local varlist = "`varlist' ecitizen enatcit"
    }
    // TMSA variable only exists in 1996/2001 panels
    if `panel' == 1996 | `panel' == 2001 {
        local varlist = "`varlist' tmsa"
    }

    // obtain canonical file name (with a wildcard to match all wave indices)
    // used to save relevant wave files in import routines.
    dbg_off sipp_gen_fname, panel(`panel') dataset("core") wildcard

    // find imported core files present on disk, sort alphabetically
    local fn_waves: dir "${nber_out_dir}" files "`r(fname)'"
    local fn_waves: list sort fn_waves

    foreach fn of local fn_waves {
        dbg_off quiet append using "${nber_out_dir}/`fn'", keep(`varlist') `opts'
        // import notes and labels only on first iteration
        local opts = "nolabel nonotes"
    }

    // retrieve personal relationships from topical module file
    local varlist = "erelat*"
    
    // citizenship vars for 04/08 panels already retrieved above
    if `panel' == 2001 {
        local varlist = "`varlist' tcitiznt"
    }
    else if `panel' == 1996 {
        local varlist = "`varlist' rcitiznt"
    }

    // data is stored in wave 2 TM files
    dbg_off sipp_gen_fname, panel(`panel') dataset("tm") wave(2)

    dbg_off merge m:1 uuid using "${nber_out_dir}/`r(fname)'", ///
        keepusing(`varlist') assert(master match) nogenerate

    rename esex sex
    rename erace race
    rename eorigin ethnic
    rename tage age
    rename ems ms
    rename edisabl disabled
    rename renroll ststat
    rename eenrlm student
    rename eenlevel enlevel
    rename tmetro metro
    // does not exist in 2004/08 panel
    capture rename tmsa msa
    rename tfipsst sipp_st
    rename (swave srefmon) (wave refmth)


end // sipp_harmonize_b_post96

//------------------------------------------------------------------------------
// Harmonizes pre-1996 panels so they conform to common naming conventions and
// have comparable variable values. 
capture program drop sipp_harmonize_b_pre96
program define sipp_harmonize_b_pre96

    version 12
    syntax, Panel(integer) 

    dbg_off sipp_gen_fname, panel(`panel') dataset("fp")
    local fn_pullpnl =  r(fname)

    // load wavely data first, reshape by uuid/wave
    local wav_vars = "att_schl* higrade* grd_cmpl* geo_ste* ed_level*" 
    use uuid `wav_vars' using "${nber_out_dir}/`fn_pullpnl'"
    
    // 1992 panel has 2-digit wave numbers to accout for wave 10
    // Correct digits to be consistent across full panel files.
    dbg_off capture rename *(##) *(#)
    
    dbg_off reshape long att_schl higrade grd_cmpl geo_ste ed_level, i(uuid) j(wave)
    sort uuid wave
    
    tempfile fn_wave_data
    save "`fn_wave_data'", replace

    // read montly data, reshape into long for using uuid/month
    // make sure we do not import mst_rgc variable, so do not use ms*
    local mth_vars = "age?? ms?? enrl_mth??"
    use uuid sex race ethnicty `mth_vars' ///
        using "${nber_out_dir}/`fn_pullpnl'", clear

    // 1992 panel has 2-digit wave numbers to accout for wave 10
    // Correct digits to be consistent across full panel files.
    dbg_off capture rename *(##) *(#)

    tempvar idx_mth
    dbg_off reshape long age ms enrl_mth, i(uuid) j(`idx_mth')
    
    generate byte refmth = mod(`idx_mth' - 1, 4) + 1
    generate byte wave = ceil(`idx_mth'/4)

    sort uuid wave refmth
    // merge previously saved wavely data
    dbg_off merge m:1 uuid wave using "`fn_wave_data'", ///
        assert(match) nogenerate
   
    // drop 10th wave from 1992 panel; this was never released as core wave
    // file, we therefore cannot merge it with core data.
    if `panel' == 1992 {
        drop if wave > 9
    }

    local varlist hmetro hmsa disab
    // obtain canonical file name (with a wildcard to match all wave indices)
    // used to save relevant wave files in import routines.
    dbg_off sipp_gen_fname, panel(`panel') dataset("core") wildcard

    // find imported core files present on disk, sort alphabetically
    local fn_waves: dir "${nber_out_dir}" files "`r(fname)'"

    foreach fn of local fn_waves {
        dbg_off merge 1:1 uuid wave refmth using "${nber_out_dir}/`fn'", ///
            keepusing(`varlist') assert(master match match_update) nogenerate `opts'
        // import notes and labels only on first iteration
        local opts = "nolabel nonotes update"
    }

    rename (ethnicty att_schl enrl_mth geo_ste hmetro hmsa disab) ///
        (ethnic ststat student sipp_st metro msa disabled)

    dbg_off recode disabled ststat (0 = -1)

    // keep only those obs. that are also present in Set A (those include only
    // obs. with pp_mis = 1 and non-missing obs. in core wave files)

    // get name of Set A file
    sipp_set_name, panel(`panel') setname("A")
    dbg_off merge 1:1 uuid wave refmth using "${extracts_dir}/`r(fname)'", ///
        keepusing(pp_mis) nogenerate

    // Merge in some variable from the topical module 2 files. Merge vars relating to country of origin
    // and whether someone is a naturalized citizen with all obs; merge vars related to education 
    // only for wave 2 as education level will not stay constant for many obs.
    // VARIABLES
    // tm8732      checkitem for country of origin 
    // tm8734       naturalized citizen?
    // tm8400       was highest attended grade 12 or less
    // tm8408       received highschool diploma?
    // tm8416       highest grade attended at least one year of college?
    // tm8422       highest degree beyond a highschool diploma earned
    // tm8430       check item: degree received higher than bachelor's degree?

    dbg_off sipp_gen_fname, panel(`panel') dataset("tm") wave(2)
    local fn_tm = "${nber_out_dir}/`r(fname)'"

    dbg_off merge m:1 uuid using "`fn_tm'", assert(master match) nogenerate /// 
        keepusing(tm8732 tm8734) 

    dbg_off merge m:1 uuid wave using "`fn_tm'", assert(master match) /// 
        keepusing(tm8400 tm8408 tm8416 tm8422 tm8430) nogenerate
    
    drop if pp_mis != 1
    drop pp_mis

end // sipp_harmonize_b_pre96

// vim: set sts=4 sw=4 et:
