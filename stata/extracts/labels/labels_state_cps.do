// Value label definitions for US States according to CPS scheme.
// Should be -include-d in sipp_process_b.do

// FIXME: CEPR copyright
// FIXME: licensing

#delimit ;
label define lbl_state_cps 
    11  "ME" 
    12  "NH" 
    13  "VT" 
    14  "MA" 
    15  "RI" 
    16  "CT" 
    21  "NY" 
    22  "NJ" 
    23  "PA" 
    31  "OH" 
    32  "IN" 
    33  "IL" 
    34  "MI" 
    35  "WI" 
    41  "MN" 
    42  "IA" 
    43  "MO" 
    44  "ND" 
    45  "SD" 
    46  "NE" 
    47  "KS" 
    51  "DE" 
    52  "MD" 
    53  "DC" 
    54  "VA" 
    55  "WV" 
    56  "NC" 
    57  "SC" 
    58  "GA" 
    59  "FL" 
    61  "KY"
    62  "TN"
    63  "AL"
    64  "MS"
    71  "AR"
    72  "LA"						
    73  "OK"						
    74  "TX"						
    81  "MT"						
    82  "ID"						
    83  "WY"						
    84  "CO"						
    85  "NM"						
    86  "AZ"						
    87  "UT"						
    88  "NV"						
    91  "WA"						
    92  "OR"						
    93  "CA"						
    94  "AK"						
    95  "HI"						
    ;
#delimit cr


// Some additional categories exist for pre-2004 panels

if `panel' <= 1993 {
    #delimit ;
    label define lbl_state_cps
        101  "ME, VT"						
        102  "IA, ND, SD"						
        103  "AK, ID, MT, WY",
        add
    ;
    #delimit cr
}
else if `panel' == 1996 | `panel' == 2001 {

    #delimit ;
    label define lbl_state_cps 
        101 "ME, VT" 
        102 "WY, ND, SD", 
        add
        ;
    #delimit cr
}

// vim: sts=4 sw=4 et:
