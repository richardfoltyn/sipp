/*
Creates job spell start and end dates (where available) using Stata's date
format.

This file should be -run- from sipp_process_d.do

Job spell start and end dates will be stored in {e,s}jdate. Wave-specific job
start and end dates (from 1990-1993 core files) will be stored in w{e,s}jdate.
*/

// Author: Richard Foltyn
// FIXME: licensing

version 12

// Argument: 4-digit panel year
args panel


//------------------------------------------------------------------------------
// COMPILE JOB SPELL START / END DATES 

/*
Data on job spell start and end dates differs fundamentally between pre- and
post-1996 panels. In 1996+ panels, these are surveyed and reported in TSJDATE /
TEJDATE. 

In 1990-1993 panels, job start dates are not surveyed for ongoing job
spells in the core questionaire. For some observations, job start dates
(month/year) are available in the first topical module (wave 2 for 1990/91
panels, wave 1 for 1992/92 panels).
Additionally, if respondent has not held job for entire reference period (wave),
start and end dates THAT RELATE TO REFERENCE WAVE ONLY are surveyed. These can
be used to determine job start and end dates in combination with other
variables. However, since only the start and end day/month are reported, we
have to manually match these to the correct year, which can differ from the
year of the refenrence month. For example, consider this fictitios data set:

WAVE    REFMTH  WSDAY   WSMONTH     MONTH   YEAR
3       1       1       12          11      1990
3       2       1       12          12      1990
3       3       1       12          1       1991
3       4       1       12          2       1991

Clearly, we cannot construct the wave-specific job start date as

    wsjdate = mdy(wsmonth, wsday, year)

as this would use the wrong year for reference months 3 and 4.
Instead we have to create a look-up table that, for a given wave/month
combination, stores the correct year. 
*/


if `panel' <= 1993 {

    preserve

    // Determine the years corresponding to w{e,s}month by creating a lookup table which contains 
    // all wave / rot / month / year combinations for particular panel.
    keep wave rot refmth month year
    duplicates drop wave rot refmth, force
    // create starting/ending dates which point to correct year for a
    // a given wave/rotation/month combination
    generate byte wsmonth = month
    generate byte wemonth = month
    generate int wsyear = year
    generate int weyear = year

    tempfile date_map
    save "`date_map'", replace

    sort wave rot refmth
    list wave rot refmth month year

    // merge generated date map so that starting/ending years correctly
    // correspond to job starting/ending months
    restore

    // note that there will be some obs. from using only, as some
    // wave/rot/w{e,s}month combination may not be observed in data set
    dbg_off merge m:1 wave rot wsmonth using "`date_map'", keep(master match) nogenerate keepusing(wsyear)
    dbg_off merge m:1 wave rot wemonth using "`date_map'", keep(master match) nogenerate keepusing(weyear)

    generate wsjdate = mdy(wsmonth, wsday, wsyear)
    generate wejdate = mdy(wemonth, weday, weyear)
    drop w*month w*year w*day

    // merge starting dates from topical modules
    preserve

    local widx = cond(`panel' == 1992 | `panel' == 1993, 1, 2)
    local varlist = "uuid tm8214 tm8218 tm8220 " + cond(`panel' == 1990 | `panel' == 1991, "tm8206", "")

    dbg_off sipp_gen_fname, panel(`panel') dataset("tm") wave(`widx')
    use `varlist' using "${nber_out_dir}/`r(fname)'", clear

    rename tm8214 job

    if `panel' == 1990 | `panel' == 1991 {
        replace job = tm8206 if tm8206 > 0 
        drop tm8206
    }

    // create sdate in date format to be compatible with 1996+ panels, 
    // even if we do not know day in this case.
    generate sjdate = dofm(ym(tm8220, tm8218))

    // if no job present, this is coded as 0 in tm files
    drop if job <= 0

    tempfile tm_job_data
    save "`tm_job_data'", replace

    restore

    dbg_off merge m:1 uuid job using "`tm_job_data'", keep(master match) /// 
        keepusing(sjdate)

    // flags to indicate how start/end dates were obtained
    // See the end of this files for valid values.
    generate byte flag_ejdate = .
    generate byte flag_sjdate = .

    replace flag_sjdate = 3 if _merge == 3
    drop _merge

    preserve

    // construct missing sjdate / ejdate from wsjdate / wejdate where possible
    // We could attempt to use WS#CHG variables to identify when job spells
    // start or end, but these do not always seem to correspond to what is
    // reported in wsjdate/wejdate.

    keep if !missing(job)

    sort uuid job wave refmth
    collapse (first) ersend estlemp wejdate wsjdate sjdate epppnum ///
        wave_start, by(uuid job wave) fast

    // regenerate flags here so we can merge them with main data set
    generate byte flag_sjdate = .
    generate byte flag_ejdate = .

    // assume that job spell ended if some reason for stopping work was
    // provided.
    by uuid job: replace flag_ejdate = 1 if !missing(wejdate[_N]) ///
         & !missing(ersend[_N]) & ersend[_N] > 0 

    by uuid job: generate ejdate = wejdate[_N] if flag_ejdate == 1

    /*
        Start dates reported in topical module and those given in
        WS###16/WS###18 often deviate.
        Since we do not know which one is more plausible, use topical module as
        primary source.

        Inferring the job starting dates from WS###16/WS###18 is much less
        straightforward than for job end dates, as we have no such variable as
        ERSEND which indicates that a new job was indeed taken up.

        Even if WS###16/WS###18 are present, this does not imply that a new job
        was started. Some things to keep in mind:
            - WS###16/WS###18 are only surveyed if a person has not worked on a
              job for entire reference period (wave). I.e. if someone started a
              new job on the 1st day of a wave and held that job throughout the
              wave, WS###16/WS###18 will be blank.
            - WS###16/WS###18 will be reported if job TERMINATED before
              the end of a wave, even if the worker was not newly hired during
              this wave. WS###16/WS###18 are present if and only if WS###20 /
              WS###22 are present.
            - The above variables will also be reported if someone was not
              employed on this job during the entire wave, for whatever reason
              (e.g. temporary layoff?). Some job spells in the data report these
              variables repeatedly even if the job ID remains constant across
              waves.
    */

    // first wave a given person was observed can be obtained from first digit
    // of pnum
    tempvar first_wave
    generate `first_wave' = floor(epppnum / 100)

    tempfile fn_sjdate

    by uuid job: replace flag_sjdate = 4 if missing(sjdate) & wave[1] > `first_wave'  ///
        & missing(wsjdate[1])

    by uuid job: replace sjdate = dofm(wave_start[1]) if flag_sjdate == 4

    by uuid job: replace flag_sjdate = 1 if missing(sjdate) & wave[1] > `first_wave' ///
        & !missing(wsjdate[1])

    by uuid job: replace sjdate = wsjdate[1] if flag_sjdate == 1
    
    // FIXME: remove in final version
    by uuid job: assert (sjdate == sjdate[1]) & (ejdate == ejdate[1])

    // sort order irrelevant, variables are constant within uuid-job groups
    collapse (first) sjdate ejdate flag_*jdate, by(uuid job) fast

    tempfile fn_wjdates
    save "`fn_wjdates'", replace

    restore
    merge m:1 uuid job using "`fn_wjdates'", nolabel nonotes nogenerate ///
        assert(match master match_update) update 

}
else if `panel' >= 1996 {
    // In the 1996+ panels, job spell start and end dates are stores in
    // t{e,s}jdate variables using format YYYYMMDD.
    // We convert these to Stata date format.
    // Note also the SIPP User Notes about errors in these variables in the 1996 panel.
    foreach sif of newlist sjdate ejdate {
        // existing variable containing human-readable format
        local hrf = "t`sif'"
        generate `sif' = date(string(`hrf', "%8.0f"), "YMD")

        // Occassionally, the dates reported in t{s,e}jdate are invalid, in
        // particular because the reported day of month is too high, e.g. 2010-02-30. 
        // In such cases date() creates a missing observation.
        // Correct this by building a date with the next-period month, converting it to date format and 
        // subtract 1 to arrive at a valid last day of the month.
        // However, there are other dates where the month is 00. These will be converted to missing dates,
        // as there is no straighforward way to guess what the correct month might have been.
        replace `sif' = dofm(mofd(date(string(floor(`hrf'/100) , "%6.0f") +  "01", "YMD")) + 1) - 1 ///
            if missing(`sif') & !missing(`hrf')

    }

    drop tsjdate tejdate
}

//------------------------------------------------------------------------------
// FIX CONFLICTING / MISSING JOB START / END DATES 

/*
    The following job spell start and end date clean up code only applies to
    1996+ panels. The problem addressed below (inconsistencies across waves)
    cannot arise in pre-1996 panels conceptually, as t}
here 
        1. no job end dates are reported;
        2. job start dates are obtained from the topical module in a single
            wave, and thus cannot be inconsistent across waves.

    Potential problems with job spell start and end dates:
    
    - Due to lack of longitudinal editing, job spells that ended while the panel
      was still in progress only have end dates in wave that includes end date
      (and possibly the prior wave)
      Fix: Add job end date to earlier waves (replace missing values).
    - Non-constant (non-missing) start dates across waves: job start dates
      occasionally differ by several years, probably due to wrong hot-deck
      imputation.
      Fix: replace all conflicting imputed start dates with longitudinally
      consistent ones.
    - Non-constant end date across waves: Replace with last observation if
      deviation occurs only in last-but-one wave (to allow for updated
      information). 
*/

if `panel' >= 1996 {
    preserve

    // throw away all obs. without jobs, collapse to wavely frequencies as job
    // start and end dates are constant within waves.
    keep if !missing(job)

    sort uuid job wave
    collapse (first) refmth refdate sjdate ejdate asjdate, ///
        by(uuid job wave) fast

    // flags to indicate how start/end dates were obtained
    // See the end of this files for valid label values.
    generate byte flag_ejdate = .
    generate byte flag_sjdate = .

    /*
    First replace missing job end dates from last wave in prior waves.
    Take into account that last-but-one wave might contain slightly different
    job end date in 1996+ panels -- replace it with last observation.
    
    Criteria:
        1. At least two job-month observations present
        2. last job end date obs. of job spell must be nonmissing
        3. last-but-one job end date obs. may or may not have conflicting value to
            allow for updates in final wave of job spell.
        4. all other prior waves must have missing job end date.
    
    In this case replace all job end dates by the last observation. In all other
    cases (e.g. with more conflicting non-missing values) it is not straightforward
    what to do, leave data as is.
    */

    tempvar edt_ejdate1 edt_ejdate2 
    by uuid job: gen byte `edt_ejdate1' = missing(ejdate) | _n >= (_N-1) ///
        if !missing(ejdate[_N]) & _N != 1

    by uuid job: egen `edt_ejdate2' = min(`edt_ejdate1')
    
    // Do not consider replacing missing values as imputation, so do not indicate
    // this in flag_ejdate.
    by uuid job: replace flag_ejdate = 2 if `edt_ejdate2' == 1 & ///
        !missing(ejdate) & ejdate != ejdate[_N]

    tab flag_ejdate, missing
    
    by uuid job: replace ejdate = ejdate[_N] if `edt_ejdate2' == 1

    /*
    Ensure some longitudinal consistency for job start dates.
    This affects far fewer obs. than the above code for ejdate.
    
    As SIPP does not use longitudinal editing for 1996+ panels, start values that
    were imputed (asjdate > 0) are imputed using donors from specific wave only,
    but no longitudinal consistency for a particular job spell is enforced.
    We ensure longitudinal consistency for job spell with imputed job start dates
        if:
        1. At least two job-month obs. present
        2. first obs. of job spell has nonmissing start date
        3. first start date obs. is not imputed (asjdate == 1)
        4. job start date was imputed in some later wave (asjdate > 0 for some obs.
            with _n > 1)
        5. job start date in later wave differs from first observed job start date 
    */

    tempvar edt_sjdate1 edt_sjdate2
    // check whether there are any conflicting imputed observations
    by uuid job: gen byte `edt_sjdate1' = (sjdate != sjdate[1]) & asjdate > 0 ///
        if !missing(sjdate[1]) & asjdate[1] == 0 & _N != 1

    by uuid job: egen byte `edt_sjdate2' = max(`edt_sjdate1')
    
    // Replace all observations, not just imputed ones, as later waves
    // just adopt the these values, but without imputation flag.
    // Also, replace only if resulting start date is not past end date so we do
    // not introduce new inconsistencies.
    by uuid job: replace flag_sjdate = 2 if `edt_sjdate2' == 1 & ///
        sjdate != sjdate[1] & (sjdate[1] <= ejdate)
    
    by uuid job: replace sjdate = sjdate[1] if flag_sjdate == 2


    keep uuid job wave flag_sjdate flag_ejdate sjdate ejdate
    tempfile fn_jdate_edt
    save "`fn_jdate_edt'"

    restore
    merge m:1 uuid job wave using "`fn_jdate_edt'", ///
        assert(master match match_update match_conflict) ///
        update replace nolabel nonotes nogenerate

    drop asjdate 
}

/*
Some job spells have obs. even in waves after the spell is supposed to have
ended (ersend > 0 & wejdate not missing), possibly because the
respondent was able to keep job after all. If we observe earnings, 
delete the end date and assume that job is still ongoing.
*/

sort uuid job wave refmth

tempvar after after1
generate byte `after' = (mofd(ejdate) < wave_start) ///
    if !missing(ejdate) & !missing(earn)

by uuid job: egen byte `after1' = max(`after')

replace ejdate = . if `after1' == 1
replace flag_ejdate = . if `after' == 1

// add default value of 0 to all obs with non-missing dates with have no other
// flag value
replace flag_ejdate = 0 if !missing(ejdate) & missing(flag_ejdate)
replace flag_sjdate = 0 if !missing(sjdate) & missing(flag_sjdate)

//------------------------------------------------------------------------------
// DIAGNOSTICS

// at this point we are left with potentially inconsistent job spell dates
// without a straightforward way to correct them.

tempvar nonconst conflict

sort uuid job wave refmth

by uuid job: gen byte `nonconst' = (sjdate != sjdate[1])

// tabulate non-constant start dates
tab `nonconst', missing
drop `nonconst'

by uuid job: gen byte `nonconst' = (ejdate != ejdate[1])
// tabulate non-constant end dates
tab `nonconst', missing

generate byte `conflict' = sjdate > ejdate if !missing(sjdate) & !missing(ejdate)
//tabulate how many obs have end dates before start dates
tab `conflict', missing


//------------------------------------------------------------------------------
// DROP JOB OBS. PRECEDING START DATE OR TRAILING END DATE

// First we drop all observations that are outside of the sjdate / ejdate (if
// present) time interval that can reasonably be assumed to be invalid, either
// because we have no reported earnings, or ESR > 5 indicates that person had
// no job in any of the weeks in reference month.
/*

// TODO: Probably we should not drop these observations at all, as whether they
// are useless or not depends on research interest.

sort uuid job wave refmth

tempvar ibefore earn_imp iafter

if `panel' <= 1993 {
    generate byte `earn_imp' = 1 if apmsum == 1
}
else if `panel' >= 1996 {
    generate byte `earn_imp' = 1 if apmsum > 0 & !missing(apmsum)
}

gen `ibefore' = 1 if mofd(sjdate) > refdate & !missing(sjdate) & ///
    (missing(earn) | `earn_imp' == 1 | esr > 5) ///

tab `ibefore', missing
drop if `ibefore' == 1

gen `iafter' = 1 if mofd(ejdate) < refdate & !missing(ejdate) & ///
    (missing(earn) | `earn_imp' == 1 | esr > 5)

tab `iafter', missing
drop if `iafter' == 1
*/

/*
    Tag all remaining job/month obs. outside of sjdate / ejdate (if present)
    time period.
    Such observations might exist for several reasons:
        1. For a person with 4 monthly observations per wave, reshaping creates
            4 job/month obs. regardless of whether the job was held in all 4
            months.
        2. If person started new job after the 4th reference month but before
            the interview covering that wave was conducted, that job id seems
            to be reported for the entire wave covered by that interview.
        3. The way in in which weeks are treated in SIPP can lead to
            observations being reported for reference months in which the person
            was no longer employed. 
            As an example, consider a person who held a job until June 30th
            2013, where June 2013 corresponded to a reference month other than
            4. In this case, SIPP data is edited such that June 30th is part of
            the first week of July (SIPP weeks start on Sunday, and at least 4
            days of a week have to belong to a particular month for that week
            to be considered as belonging to that month). Hence this job spell
            will show up in the July reference month as well, with an ESR of 4
            or 5 indicating that a job was not held for the entire month (if
            the person did not immediately find another job).

    Whether these observations should be dropped depends on what the researcher
    is interested in, so we keep them.
*/


generate byte flag_jdate = 0 if !missing(job)
by uuid job: replace flag_jdate = 1 if mofd(sjdate) > refdate & !missing(sjdate) ///
    & !missing(earn)
by uuid job: replace flag_jdate = 2 if mofd(ejdate) < refdate & !missing(ejdate) ///
    & !missing(earn)

//------------------------------------------------------------------------------
// LABELS
   
#delimit ;
label define lbl_flag_ejdate
    0   "Not imputed"
    1   "Imputed from core wave variables (90-93)"
    2   "Imputed from last observed job end date (1996+)"
    ;
label define lbl_flag_sjdate
    0   "Not imputed"
    1   "Imputed from core wave variables (90-93)"
    2   "Imputed from first value of TSJDATE (1996+)"
    3   "Topical module data (90-93)"
    4   "Assumed that job spell started during panel, assined day 1 of first reference month"
    ;
label define lbl_flag_jdate
    0   "Job obs. within start/end dates"
    1   "Job obs. reference date precedes reported start date"
    2   "Job obs. reference date past reported end date"
    ;
#delimit cr

label variable sjdate "Start date of job spell"
label variable ejdate "End date of job spell"

// FIXME: do we really need these variables later? 
if `panel' <= 1993 {
    label variable wsjdate "Wave-specific job start date"
    label variable wejdate "Wave-specific job end date"
    format %td w*jdate
}

label value flag_ejdate lbl_flag_ejdate
label variable flag_ejdate "Allocation flag for ejdate (1996+)"
    
label value flag_sjdate lbl_flag_sjdate
label variable flag_sjdate "Allocation flag for sjdate (1996+)"

label value flag_jdate lbl_flag_jdate
label variable flag_jdate "Indicates whether reference month is within reported start/end date of job spell"

format %td ejdate sjdate
format %1.0f flag_ejdate flag_sjdate flag_jdate

tab1 flag_sjdate flag_ejdate flag_jdate, missing

set more off


// vim: set sts=4 sw=4 et:
