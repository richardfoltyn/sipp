// Creates data set A of SIPP extracts.
// Set A containts IDs, weights, reference periods.

// Author: Richard Foltyn
// FIXME: cepr copyright
// FIXME: licensing


// TODO: Import calendar year weights for all panels and store them in
// consistently named variables.

capture program drop sipp_extract_a
program define sipp_extract_a

    syntax, Panel(integer) [SAVeas(string)]

    dbg_off sipp_assert_panel `panel'

    dbg_off clear

    if `panel' < 1996 {
        sipp_harmonize_a_pre96, panel(`panel')
    }
    else {
        sipp_harmonize_a_post96, panel(`panel')
    }

    do "sipp_process_a.do" `panel'

    sort uuid wave refmth
    order *, alphabetic
    compress

    label data "Set A: ID and Weights, Panel `panel' (Version ${extracts_version}, based on CEPR extracts 2.1.7)"
    notes _dta: "Created: `=c(current_date)', `=c(current_time)'" 

    if `"`saveas'"' != "" {
        save `"`saveas'"', replace
    }
    
    // some details for the log
    summarize

end // sipp_extract_a

// -------------------------------------------------------------------------  
capture program drop sipp_harmonize_a_post96
program define sipp_harmonize_a_post96

    version 12
    syntax, Panel(integer)
    
    // import only these variables from core files
    local varlist uuid swave srefmon ssuid spanel rhcalmn rhcalyr ///
        srotaton eentaid eppintvw /// 
        epppnum shhadid whfnwgt wffinwgt wsfinwgt wpfinwgt gvarstr 

    // obtain canonical file name (with a wildcard to match all wave indices)
    // used to save relevant wave files in import routines.
    dbg_off sipp_gen_fname, panel(`panel') dataset("core") wildcard

    // find imported core files present on disk, sort alphabetically
    local fn_waves: dir "${nber_out_dir}" files "`r(fname)'"
    local fn_waves: list sort fn_waves

    foreach fn of local fn_waves {
        // import notes and labels only on first iteration
        local opts = cond(_N == 0, "", "nolabel nonotes")
        dbg_off quiet append using "${nber_out_dir}/`fn'", keep(`varlist') `opts'
    }

    sort uuid

    // merge with longitudinal weights
    // obtain weight file name
    dbg_off sipp_gen_fname, panel(`panel') dataset("wgt")

    // a few uuids in using file cannot be matched, probably due to errors in
    // the data. Do not use assert(master match)!
    merge m:1 uuid using "${nber_out_dir}/`r(fname)'", keep(match master) ///
        nogenerate
    drop lgtkey

    // rename weight names so they correspond to naming convention from
    // 2001/2004 panels (2008 has different names for full panel weights)
    if `panel' == 2008 {
        rename lgtpn#wt lgtpnwt(#)
    }

    // create consistently named variable which holds the full panel weight for entire panel
    // so we do not have do differentiate by panel.
    // TODO: As of July 2013, the latest available longitudinal weight for the
    // 2008 panel is LGTPN2WT. Adjust name when later weights are released!
    local pnlwgtname = cond(`panel' == 2001, "lgtpnwt3", ///
        cond(`panel' == 2004, "lgtpnwt4", "lgtpnwt2"))

    if `panel' == 1996 {
        dbg_off rename lgtpnlwt lgtpnlwgt
    }
    else {
        generate double lgtpnlwgt = `pnlwgtname'
    }   

    // generate naming consistent across pre- and post-1996 panels.
    rename (rhcalyr rhcalmn) (year month)
    // adopt pre-1996 names for these variables, as they are more concise
    rename (swave spanel srotaton srefmon) (wave panel rot refmth)

end // sipp_harmonize_a_post96

//------------------------------------------------------------------------------
// Harmonizes pre-1996 panels so they conform to common naming conventions and
// have comparable variable values. 
capture program drop sipp_harmonize_a_pre96
program define sipp_harmonize_a_pre96

    version 12
    syntax, Panel(integer) 

    // pp_int* variables are per wave, i.e. we have around 8-10 of those,
    // depending on the panel. Most other longitudinal variables are per month.
    // Reshaping strategy: Reshape pp_int* into long form by uuid/wave and merge
    // with uuid/month longitudinal data.

    dbg_off sipp_gen_fname, panel(`panel') dataset("fp")
    local fn_pullpnl =  r(fname) 
    use uuid pp_int* using "${nber_out_dir}/`fn_pullpnl'"
    
    // 1992 panel has 2-digit wave numbers to accout for wave 10
    // Correct digits to be consistent across full panel files.
    dbg_off capture rename pp_intv(##) pp_intv(#)
    
    dbg_off reshape long pp_intv, i(uuid) j(wave)
    sort uuid wave
    
    tempfile fn_wave_data

    save "`fn_wave_data'", replace

    // read montly data, reshape into long for using uuid/month
    use uuid rot pp_entry pp_pnum pnlwgt varstrat pp_mis* hh_add* ///
        using "${nber_out_dir}/`fn_pullpnl'", clear

    rename hh_add(##)   hh_add(#)
    rename pp_mis(##)   pp_mis(#)

    sort uuid
    tempvar idx_mth
    dbg_off reshape long hh_add pp_mis, i(uuid) j(`idx_mth')
    
    generate byte refmth = mod(`idx_mth' - 1, 4) + 1
    generate byte wave = ceil(`idx_mth'/4)

    sort uuid wave refmth
    // merge previously saved per-wave pp_intv status
    dbg_off merge m:1 uuid wave using "`fn_wave_data'", ///
        nolabel nonotes assert(match) nogenerate
   
    // drop 10th wave from 1992 panel; this was never released as core wave
    // file, we therefore cannot merge it with core data.
    if `panel' == 1992 {
        drop if wave > 9
    }

    local varlist suid panel year month refmth hwgt fwgt swgt fnlwgt*
    // obtain canonical file name (with a wildcard to match all wave indices)
    // used to save relevant wave files in import routines.
    dbg_off sipp_gen_fname, panel(`panel') dataset("core") wildcard

    // find imported core files present on disk, sort alphabetically
    local fn_waves: dir "${nber_out_dir}" files "`r(fname)'"

    foreach fn of local fn_waves {
        dbg_off merge 1:1 uuid wave refmth using "${nber_out_dir}/`fn'", ///
            keepusing(`varlist') assert(master match match_update) nogenerate `opts'
        // import notes and labels only on first iteration
        local opts = "nolabel nonotes update"
    }

    // for pre-1996 full panel files, only those observations should be used
    // which have pp_mis = 1.
    drop if pp_mis != 1

    // some obs. seem to only exist in panel file but not in core files, even
    // if they have pp_mis = 1 (which is strange). 
    // Those have missing suid after merge and we drop them as it's not obvious
    // why they are present. 
    drop if missing(suid)

    // create consistent naming scheme across pre- and post-1996 panels
    rename hh_add shhadid
    rename varstrat gvarstr
    rename pp_intv eppintvw
    rename pp_pnum epppnum
    rename pp_entry eentaid
    rename fnlwgt wpfinwgt 
    rename hwgt whfnwgt
    rename fwgt wffinwgt
    rename swgt wsfinwgt
    rename suid ssuid
    rename pnlwgt lgtpnlwgt
    
    // consistent panel year format
    replace year = year + 1900
    replace panel = panel + 1900   

end // sipp_harmonize_a_pre96

// vim: set sts=4 sw=4 et:
