/*
Calculates the number of SIPP weeks that a given job spell was held during
reference month. Note that this is not a measure of how many weeks a person
actually worked, as it is sufficient that a person worked only a single day
for the SIPP week to be fully counted.

Weeks are obtained from sdate/edate arguments, or if none were
provided, by default from the wsjdate / wejdate variables. If these
are not present, it is assumed that a job spell lasted for all weeks of
reference month.

SIPP weeks deviate from calendar weeks in several ways:
 -  SIPP weeks start on Sunday.
 -  Within a wave (but not at its boundaries) weeks that span two reference
    months are completely assigned to the month that containes more than 3 days of
    the week.
 -  Weeks can never span more than one wave. If a fragment of a week at the
    beginning or end of a wave it shortes than 4 days, it is combined with the
    following / preceding week to form a SIPP week of 8-10 days.

Examples:

 1. Assume that August 2013 is the 4th reference month of some rotation group.
    Then Aug. 1-3 (Friday - Saturday) are merged with the July 28-31 to form the
    last week of July.
 2. Now assume that August 2013 is the 1st reference month of some rotation
    group. Because weeks can never span more than one wave, Aug. 1-3 cannot be
    merged with the last days of July to form a week. Furthermore, these 3 days
    are too short to be considered a week on their own. They are therefore
    merged with the following 7 days to form the 1st week of August runing from Aug.
    1-10.
 3. Consider a job spell with January 1994 as the 4th reference month that
    lasted from Jan. 29-31. Since Jan 30th is a Sunday, Jan 30-31st form a new
    week. Since this is the 4th reference month, it is not part of the
    following month, even though it lasted for only 2 days in January. Thus the
    resulting number of weeks is 2, even if the person worked only for 3 days!

Result:

 -  The variable created should be almost identical to WKS-EMP{1,2} variable
    from the longitudinal files for pre-1996 panels, except for a few
    instances where the reported SIPP value is wrong. However, we report the
    number of weeks as 0 if the job ID is not missing but the job spell does
    not cover any SIPP weeks in a reference month (based on wsjdate/wejdate), 
    while in the SIPP such observations are reported as missing.
 -  For the 1996 panel, the calculation of SIPP weeks in the refenrece month
    seems to be wrong for 1997m12, 1998m1 and 1998m9, so for there reference
    months in particular the results obtained here differ from one reported by
    SIPP by 1 week where a job spell lasted the whole month.
 -  Except for the above problem for the 1996+ panel, the results here differ
    from the ones reported by SIPP for about 0.4% of observations for persons
    who had a single job.
*/

// Author: Richard Foltyn
// FIXME: licensing

capture program drop sipp_dates_to_sweeks
program define sipp_dates_to_sweeks

version 12
syntax [if] [in], generate(name) [sdate(varname)] [edate(varname)]

local wksemp: copy local generate
confirm new variable `wksemp'

local sdate = cond("`sdate'" == "", "wsjdate", "`sdate'")
local edate = cond("`edate'" == "", "wejdate", "`edate'")

confirm variable `sdate' `edate' refdate

marksample touse

tempvar ms me valid

// calculate dates of first and last day of month
generate `ms' = dofm(refdate)
generate `me' = dofm(refdate + 1) - 1

tempvar jmstart jmend sun_init srefwk erefwk sun_passed

// Compute which weeks are counted as belonging to reference month. This time
// period is bounded by srefwk and erefwk dates.
generate `srefwk' = `ms' + (7 - dow(`ms')) if dow(`ms') > 3
replace `srefwk' = `ms' - dow(`ms') if dow(`ms') <= 3
replace `srefwk' = `ms' if refmth == 1

generate `erefwk' = `me' + (7 - dow(`me') - 1) if dow(`me') >= 3
replace `erefwk' = `me' - (dow(`me') + 1) if dow(`me') < 3
replace `erefwk' = `me' if refmth == 4

// Start and end dates for job spell in reference month
// If no start/end dates were provided, assume that job spell started on the
// 1st day / ended on the last day of reference month.
generate `jmstart' = max(`sdate', `srefwk') if !missing(`sdate')
replace `jmstart' = max(`ms', `srefwk') if missing(`sdate') & !missing(job)
generate `jmend' = min(`edate', `erefwk') if !missing(`edate')
replace `jmend' = min(`me', `erefwk') if missing(`edate') & !missing(job)

generate `sun_init' = `jmstart' - dow(`jmstart')
// correct for refmth 1, as here the first week can have 4-10 years. Note that
// weeks cannot span more than one wave.
replace `sun_init' = `sun_init' + 7 if `srefwk' - `sun_init' > 3 & ///
    refmth == 1

// Calculate number of passed Sundays since initial sunday. Each sunday
// corresponds to a newly begun week.
generate `sun_passed' = ceil( (`jmend' - `sun_init' + 1) / 7 ) if `touse'
generate `wksemp' = `sun_passed'

// Correct for the fact that we do not count the very first or last calendar
// week as a proper week if it has less than 4 days. The only exception is if
// the entire period bounded by jmstart - jmend is contained within this week.
replace `wksemp' = `wksemp' - 1 if refmth == 4 & ///
    (`erefwk' - `sun_init' - 7*(`sun_passed'-1)) < 3 & ///
    `erefwk' - `jmstart' > 3 & `touse' == 1

replace `wksemp' = `wksemp' + 1 if refmth == 1 & ///
    (`sun_init' - `srefwk' < 4) & `jmend' < `sun_init' & `touse' == 1

// above algorithm can produce negative if inputs make no sense, recode to
// 0 if a job was present in reference month.
replace `wksemp' = 0 if `wksemp' < 0 & `touse' == 1 & !missing(job)

end // sipp_dates_to_sweeks

// vim: set sts=4 sw=4 et:
