// Ensures that a list of values is nondecreasing. 
// Any values that are strictly smaller than a preceding value in a list grouped 
// by the variables specified in the respective option will be replaced
// with the last-observed value.

// Author: Richard Foltyn
// FIXME: licensing

capture program drop sipp_nondecr
program define sipp_nondecr, byable(onecall)

    syntax varname [, GENerate(name) TAG(name) REPLACE MAXITER(integer 100)]

    if `"`generate'"' != "" & `"`replace'"' != "" {
        display "Options GENERATE and REPLACE are mutually exclusive!"
        error 184 
    }

    tempvar old new
    gen `old' = `varlist'
    gen `new' = `varlist'

    // check whether we were called with -by- prefix.
    if `"`_byvars'"' != "" {
        local lgroupby = "by `_byvars': "
    }

    local ii = 1
    while 1 {
        display "Iteration `ii'"
        quietly replace `old' = `new'
        `lgroupby' replace `new' = `old'[_n-1] if _n > 1 & !missing(`old'[_n-1]) & ///
            (`old' < `old'[_n-1] | missing(`old'))
        capture assert `new' == `old', fast
        // break if assertion is true
        if ( _rc == 0 ) continue, break
        local ++ii
        // should definitely converge in less than 100 iterations, 
        // as we have no 100 months of obs. per person
        assert `ii' <= `maxiter'
    }

    if `"`tag'"' != "" {
        generate byte `tag' = (`varlist' != `new') if !missing(`varlist')

        #delimit ;
        label define lbl_`tag'
            0   "Not edited"
            1   "Edited to ensure nodecreasing values"
            ;
        #delimit cr
        label value `tag' lbl_`tag'
    }

    if `"`generate'"' != "" {
        local ltype: type `varlist'
        local lformat: format `varlist'
        gen `ltype' `generate' = `new'
        format `lformat' `generate'
    }
    else if `"`replace'"' != "" {
        replace `varlist' = `new'
    }

end // sipp_nondecr
