// Generates file names based on panel year, data set and wave index. The
// idea is that naming conventions should be consistent and determined in one
// place (this file).

// Author: Richard Foltyn
// FIXME: licensing

capture program drop sipp_gen_fname
program define sipp_gen_fname, rclass

    syntax, Panel(integer) DATaset(string) [Wave(integer 0)] [WIldcard]

    dbg_off sipp_assert_panel `panel'
    dbg_off sipp_assert_dataset `dataset'

    local dataset = lower("`dataset'")

    if "`dataset'" == "core" {
        local suffix = "w"
    }
    else if "`dataset'" == "tm" {
        local suffix = "t"
    }
    else if "`dataset'" == "fp" {
        local suffix = "fp"
    }
    else if "`dataset'" == "wgt" {
        // do not name "wgt", otherwise core file names with wildcard might
        // match weight files too.
        local suffix = "lwgt"
    }

    // make sure wave index is in 2-digit-format with leading zeros if
    // necessary; This is useful as alphabetically sorting file names will
    // ensure that wave files are in correct order, instead of 10, 11, 12, etc.
    // being processed before 2, 3, ... 
    // Skip wave altogether if none is given.
    local wave = string(`wave', "%02.0f")
    local wave = cond("`wave'" == "00", "", "`wave'")
    
    // add wildcard instead of wave which can be used in -macro dir- to locate
    // files for particular panel/data set
    if "`wildcard'" != "" {
        local wave = "*"
    }
     
    return local fname = "sipp`panel'`suffix'`wave'.dta"


end // sipp_gen_fname

// vim: sts=4 sw=4 et:
