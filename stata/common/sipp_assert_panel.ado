
capture program drop sipp_assert_panel
program define sipp_assert_panel

    args pid

    local pid_valid "1990 1991 1992 1993 1996 2001 2004 2008"
    local is_valid: list pid in pid_valid

    if `is_valid' == 0 {
        display as error "Invalid panel id: `pid'" 
        error 9
    }

end
