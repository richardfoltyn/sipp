// Generates file names for uniform SIPP extract files.
// To ensure that names are consistent across all do-files, we generate them
// here.

capture program drop sipp_set_name
program define sipp_set_name, rclass

    syntax, Panel(integer) SETname(string)

    // update to reflect which set letters are valid
    local set_valid = "a b d"
    local setname = lower("`setname'")
    local is_valid: list setname in set_valid

    if `is_valid' == 0 {
        display as error "Invalid set name. Must be one of `set_valid'"
        exit 9 
    }

    return local fname = "sipp`panel'_set_`setname'.dta"
    

end // sipp_set_name

// vim: set sts=4 sw=4 et:
