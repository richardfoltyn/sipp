// Creates subsample from imported NBER SIPP files
// Can be used to test Stata code on smaller subsample of SIPP data to speed up
// development.

local home: environment HOME
local dataroot = "/Data/SIPP"

// base directory of the git repository containing all SIPP code
local git_repo = "`home'/repos/sipp"
local log_dir = "`home'/stata/log"

local nber_out_dir = "`dataroot'/dta"
// Target directory where to save subsample data
local sample_out_dir = "`dataroot'/dta_sample"

// Add directory with commonly used commands
adopath + "`git_repo'/stata/common"

capture log close

set more off
set trace off
set tracedepth 1

clear *

log using "`log_dir'/sipp_sample.log", replace

// ensure that selected subsample is identical across invocations
set seed 1234

// percentage of individuals to keep in subsample
local smpl_size 5

// for pre-1996 panels, randomly select UUIDs from full panel file and drop all
// obs. in core and topical files that do not correspond to these UUIDs.
foreach pid of numlist 1990/1993 {
    
    dbg_off sipp_gen_fname, panel(`pid') dataset("fp")
    local fn_fullpnl = r(fname)

    use using "`nber_out_dir'/`fn_fullpnl'", clear

    tempvar keep
    dbg_off egen byte `keep' = anymatch(pp_mis*), value(1)
    drop if `keep' != 1

    dbg_off sample `smpl_size'
    save "`sample_out_dir'/`fn_fullpnl'", replace
    dbg_off describe, short

    keep uuid
    preserve

    sipp_gen_fname, panel(`pid') dataset("core") wildcard
    local files_core: dir "`nber_out_dir'" files "`r(fname)'"
    foreach fn of local files_core {
        restore, preserve
        merge 1:m uuid using "`nber_out_dir'/`fn'", nogenerate ///
            keep(match) assert(master match)
        save "`sample_out_dir'/`fn'", replace
        dbg_off describe, short
    }

    sipp_gen_fname, panel(`pid') dataset("tm") wildcard
    local files_tm: dir "`nber_out_dir'" files "`r(fname)'"

    foreach fn of local files_tm {
        restore, preserve
        merge 1:m uuid using "`nber_out_dir'/`fn'", nogenerate ///
            keep(match) assert(master match)
        save "`sample_out_dir'/`fn'", replace
        dbg_off describe, short
    }

    restore, not
}

// for 1996+ panels, just pick a random sample of UUIDs from first core wave
// file and drop obs. in topical/weight files accordingly.

clear *

foreach pid of numlist 1996 2001 2004 2008 {
    
    dbg_off sipp_gen_fname, panel(`pid') dataset("core") wave(1)

    use uuid using "`nber_out_dir'/`r(fname)'", clear
    dbg_off duplicates drop uuid, force
    dbg_off sample `smpl_size'

    preserve

    dbg_off sipp_gen_fname, panel(`pid') dataset("core") wildcard
    local files_core: dir "`nber_out_dir'" files "`r(fname)'"
    foreach fn of local files_core {
        restore, preserve
        merge 1:m uuid using "`nber_out_dir'/`fn'", nogenerate ///
            keep(match)
        save "`sample_out_dir'/`fn'", replace
        dbg_off describe, short
    }

    sipp_gen_fname, panel(`pid') dataset("tm") wildcard
    local files_tm: dir "`nber_out_dir'" files "`r(fname)'"
    foreach fn of local files_tm {
        restore, preserve
        merge 1:m uuid using "`nber_out_dir'/`fn'", nogenerate ///
            keep(match)
        save "`sample_out_dir'/`fn'", replace
        dbg_off describe, short
    }

    dbg_off sipp_gen_fname, panel(`pid') dataset("wgt")
    local fn_wgt = r(fname)
    restore, preserve
    merge 1:m uuid using "`nber_out_dir'/`fn_wgt'", nogenerate ///
        keep(match)
    save "`sample_out_dir'/`fn_wgt'", replace
    dbg_off describe, short

    restore, not
}



log close
