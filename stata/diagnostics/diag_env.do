// Global paths for data, program, etc. directories

//------------------------------------------------------------------------------
// get home directory
local home: environment HOME
local dataroot = "/Data/SIPP"

// base directory of the git repository containing all SIPP code
local sipp_repo = "`home'/repos/sipp"

global log_dir = "`home'/stata/log"

global graph_dir = "`sipp_repo'/graphs"


// ------------------------------------------------------------
// PROGRAM DIRECTORIES
// Add ado-files in common/ to ADO path so we do not need to prefix them with
// directory.

adopath + "`sipp_repo'/stata/common"

// ------------------------------------------------------------
// Extract directory
global extracts_dir = "`dataroot'/extracts"
