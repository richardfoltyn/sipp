// Computer the population universe for earch panel, compares results to time
// series from FRED (restricted to persons aged 16 and over).

// Author: Richard Foltyn
// FIXME: licensing

capture program drop diag_universe
program define diag_universe

    version 12
    syntax [, saveas(string)]
   
    clear

    // CNP16OV - civilian noninstitutional population aged 16+
    freduse CNP16OV

    rename CNP16OV pop_fred
    label variable pop_fred "Civilian noninstitutional population aged 16+ (source: FRED)"

    // rescale to get population in persons
    replace pop_fred = pop_fred * 1000
    generate refdate = mofd(daten)
    keep pop_fred refdate

    tempfile fn_pop_fred
    save "`fn_pop_fred'", replace

    local panels 1990/1993 1996 2001 2004 2008

    clear *
    // append set A for all requesed panels
    foreach pid of numlist `panels' {
    
        sipp_set_name, panel(`pid') setname("a")
        quiet append using "${extracts_dir}/`r(fname)'", `opts'
        local opts = "nolabel nonotes"
    }

    // Note: When running -merge- below, it is essential to include the update
    // option, as otherwise variables from sets other than A will be missing
    // for all but the first panel!

    // merge set B -- needed to identify those over 16
    foreach pid of numlist `panels' {
        sipp_set_name, panel(`pid') setname("b")
        local fn_set_b = r(fname)

        merge 1:1 uuid wave refmth using "${extracts_dir}/`fn_set_b'", ///
            assert(master match match_update) nogenerate update ///
            keepusing(age)
    }

    preserve 

    // Determine the number of rotation groups per panel/year/month
    // This varies at beginning / end of panel
    keep panel year month rot
    sort panel year month
    egen byte tag_rot = tag(panel year month rot)
    collapse (sum) pym_nrot=tag_rot, by(panel year month)
    
    list panel year month pym_nrot, nolabel sepby(panel)
    
    tempfile fn_pym_nrot
    save "`fn_pym_nrot'", replace

    restore

    merge m:1 panel year month using "`fn_pym_nrot'", ///
        assert(match) keepusing(pym_nrot) nogenerate

    replace wpfinwgt = wpfinwgt * (4 / pym_nrot)

    collapse (sum) pop_sipp=wpfinwgt (first) refdate if age >= 16, by(panel year month)

    merge m:1 refdate using "`fn_pop_fred'", keep(match) assert(match using) ///
        nogenerate

  
    quietly summarize refdate

    local sdate = r(min)
    local edate = r(max)

    foreach pid of numlist `panels' {
        generate double pop_sipp_`pid' = pop_sipp if panel == `pid'
        label variable pop_sipp_`pid' "SIPP `pid'"
    }

    sort panel refdate

    #delimit ;
    graph twoway (line pop_fred refdate)
            (line pop_sipp_1990 refdate)
            (line pop_sipp_1991 refdate)
            (line pop_sipp_1992 refdate)
            (line pop_sipp_1993 refdate)
            (line pop_sipp_1996 refdate)
            (line pop_sipp_2001 refdate)
            (line pop_sipp_2004 refdate)
            (line pop_sipp_2008 refdate),
        legend(label(1 "FRED"))
        ylabel(2e8 "200m" 2.2e8 "220m" 2.4e8 "240m")
        title("Civ. noninst. population aged 16+")
        tlabel(`sdate'(36)`edate', format(%tm))
        xtitle("")
        ;
    #delimit cr

    if `"`saveas'"' != "" {
        graph export "`saveas'", replace
    }

end // diag_universe

// vim: sts=4 sw=4 et:
