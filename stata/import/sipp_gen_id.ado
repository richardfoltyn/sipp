// Creates IDs for each person that are unique within a particular panel.

// Author: Richard Foltyn
// FIXME: licensing

/* 
    Overview of identification in SIPP:

    1996+ panels:
    - SSUID (12 digits) + EPPPNUM (4 digits) uniquely identify person within a
      particular panel. EENTAID (entry address ID) is not required, therefore
      we ignore it.
    
    pre-1996 panels:
    - SUID (9 digits) + ENTRY (2 digits) + PNUM (3 digits) uniquely identify a
      person within a particular panel in core wave files.
    - ID (9 digits) + ENTRY(2 digits) + PNUM (3 digits) uniquely identify a
      person within a particular panel in topical module files.
    - PP_ID (9 digits) + PP_ENTRY (2 digits) + PP_PNUM (3 digits) uniquely
      identify a person within a particular panel in full panel files.
      In 1992 panel, due to a 10th wave the field widths are PP_ENTRY(3) and
      PP_NUM(4).

    Algorithm to create UUIDs for given file:
    - UUIDs are stored as str23, as double cannot precisely store more than 16
      digits.
    - Some fields are padded with zeros as needed to ensure that the UUID field
      width is always 23 characters.
    - The resulting UUIDs are:
        - for 1996+ panels:
            YYYY-0SSUID-EPPPNUM
        - for pre-1996 core files:
            YYYY-SUID-0ENTRY-0PNUM
        - for pre-1996 topical files:
            YYYY-ID-0ENTRY-0PNUM
        - for 1990, 1991 and 1993 full panel files:
            YYYY-PP_ID-0PP_ENTRY-0PP_PNUM
        - for 1992 full panel file:
            YYYY-PP_ID-PP_ENTRY-PP_PNUM
*/
capture program drop sipp_gen_id
program define sipp_gen_id

syntax , [Panel(integer 0)] DATaset(string) [GENerate(name)]

sipp_assert_dataset `dataset'

// call the generated ID variable pid (person ID) by default, as 'ID' is already
// used in some topical module files
local lid = cond(`"`generate'"' == "", "uuid", "`generate'")

// for some data sets such as pre-1996 topical module and full panel files
// there is no panel variable present, thus we need to obtain it from the
// optional argument. In all other cases, obtain panel year directly from
// variables in data.
if `panel' > 0 {
    sipp_assert_panel `panel'
    local panelid = `panel'
}
else {
    // variable that contains panel year is namens SPANEL in 1996+ panels and PANEL
    // in earlier ones.
    capture confirm variable spanel
    if _rc == 0 {
        local panelid = spanel[1]
    }
    else {
        capture confirm variable panel
        if _rc == 0 {
            // some files use only last 2 year digits, others (such as the jid
            // files) use 4 digits. 
            local panelid = cond(panel[1] < 100, 1900 + panel[1], panel[1])
        }
    }
}

// at this point we should have valid panel years for all observations
assert `panelid' > 0 & `panelid' < .

// fix inconsistent naming conventions in pre-1996 full panel and topical
// module files.
local lsuid  = "suid"
local lentry = "entry"
local lpnum = "pnum"

if "`dataset'" == "tm" {
    local lsuid = "id"
}
else if "`dataset'" == "fp" {
    local lsuid = "pp_id"
    local lentry = "pp_entry"
    local lpnum = "pp_pnum"
}

tempvar pid
gen str4 `pid' = "`panelid'"

if `panelid' >= 1996  {
    tempvar str_ssuid str_epppnum
    // ensure that IDs are formatted with leading zeros
    // Make SSUID 13 digits wide so that overall UUID lenght including
    // separating dashes is str23 for ALL panels.
    format %013.0f ssuid
    format %04.0f epppnum
    quietly dbg_off tostring ssuid epppnum, generate(`str_ssuid' `str_epppnum') usedisplayformat
    dbg_off egen str23 `lid' = concat(`pid' `str_ssuid' `str_epppnum'), ///
        punct("-") maxlength(21)
}
else if `panel' <= 1993 {
    tempvar str_suid str_entry str_pnum

    // ensure that IDs are formatted with leading zeros. Take into account that
    // in 1992 full panel file, PP-PNUM is 4 digits and PP-ENTRY is 3 digits
    // due to a 10th wave. Format all ENTRY and PNUM variables using same field
    // width.

    format %09.0f `lsuid'
    format %04.0f `lpnum'
    format %03.0f `lentry'

    quietly dbg_off tostring `lsuid' `lentry' `lpnum', generate(`str_suid' `str_entry' `str_pnum') ///
        usedisplayformat 

    dbg_off egen str23 `lid' = concat(`pid' `str_suid' `str_entry' `str_pnum'), ///
        punct("-") maxlength(21)
}

label variable `lid' "Universally-unique person ID (across panels)"

end // sipp_gen_Id

// vim: set sts=4 sw=4 et:
