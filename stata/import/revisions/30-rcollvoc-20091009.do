// Applied minor errors in RCOLLVOC for the 2008 panel, wave 1.
// See the SIPP user notes at 
// http://www.census.gov/sipp/core_content/core_notes/2008-Panel-Wave-1-Core-Education-User-Note.html

// Author: Richard Foltyn
// FIXME: licensing

capture program drop sipp_apply_rev
program define sipp_apply_rev

    syntax , Panel(integer) DATaset(string) [Wave(integer 0)]

    if `panel' != 2008 | `wave' != 1 | `"`dataset'"' != "core" {
        exit
    }

    // maybe we should do this globally?
    set seed 1234

    tempvar rnd

    // We opt for the solution to randomly assign the value 10 to 83% of
    // affected observations and a value of 5 to the remaining 17%.
    // Alternatively, we could assign 10 to all affected obs.
    generate byte `rnd' = rbinomial(1, 0.83)
    replace rcollvoc = `rnd' * 10 + (1-`rnd') * 5 if evocat == 1 & rcollvoc == -1

end
