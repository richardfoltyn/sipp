/*
Applies the revision to job IDs for the 1990-1993 panels. See the SIPP users
notes at 
http://www.census.gov/sipp/core_content/core_notes/id_editing.html
for details.
Revisions are applied to the core, topical and full panel data sets.
Revisions affect all core files of 1990-1993 panels; all full panel files;
and the topical module files for wave 2 (1990/1991 panels) and wave 1
(1992/1993 panels).

NOTE: Changing job IDs can invalidate other imputed variables. In particular,
ws###03, which indicates whether the same job was held in the previous wave, will
no longer be valid if the job ID of two consecutive waves changed.
This problem cannot be fixed within each wave file separately, but requires
longitudinal editing. If you merge several wave files you are responsible for
ensuring that ws###03 and potentially other variables are consistent!

*/
// Author: Richard Foltyn
// FIXME: license

capture program drop sipp_apply_rev
program define sipp_apply_rev

    version 12

    syntax , Panel(integer) DATaset(string) [Wave(integer 0)]

    dbg_off sipp_assert_dataset "`dataset'"

    if `panel' > 1993 | ("`dataset'" != "core" & "`dataset'" != "fp" & ///
        "`dataset'" != "tm" ) {
        // only applies to pre-1996 core, topical module and full panel files
        exit
    }

    // for topical modules, this revision applies only to the following waves:
    // For 1990/1991 panels: wave 2
    // For 1992/1993 panels: wave 1 
    if "`dataset'" == "tm" & ((`panel' <= 1991 & `wave' != 2) | ///
        (`panel' >= 1992 & `wave' != 1)) {
        exit
    }

    tempfile fn_jid fn_orig
    tempvar jobidx mthidx
    
    // As a general strategy, keep only variables of interest, otherwise reshapes
    // take ages for some panels. Merge updated data with original data set at the
    // end.
    // Save for final merge
    save "`fn_orig'"
   
    // FIXME: for debugging purposes, assert later that after all reshapes we
    // have the same number of observations.
    local obs = _N

    if "`dataset'" == "core" {
    
        keep uuid wave refmth ws12002 ws22102
        
        // convert to long format with job/month observations, rename variables to
        // correspond to names present in job id data files
        rename (ws12002 ws22102) (jobid1 jobid2)

        reshape long jobid, i(uuid wave refmth) j(`jobidx')
    }
    else if "`dataset'" == "tm" {
        // Topical modules for 1990 and 1991 panels were first done in wave 2, so it seems that the jobid
        // in these files refers either to the job held in wave 1 or to wave 2. The jobid that refers to wave 1
        // is stored in tm8206, while the jobid refering to the wave 2 4-month period is stored in tm8214
        
        local varlist = "uuid wave tm8214 " + cond(`panel' == 1990 | `panel' == 1991, "tm8206", "")
        
        keep `varlist'

        generate byte jobid = tm8214

        if `panel' == 1990 | `panel' == 1991 {
            // there should not be 2 job IDs present at the same time
            assert (tm8206 != 0 & jobid != 0) == 0
            replace jobid = tm8206 if tm8206 > 0
            replace wave = 1 if tm8206 > 0
        }
    }
    else if "`dataset'" == "fp" {
        keep uuid jobid1* jobid2*

        // rename variables to eliminate preceeding 0's for months 1-9,
        // otherwise reshape does not find these variable names
        rename jobid?(##) jobid?(#)
    
        reshape long jobid1 jobid2, i(uuid) j(`mthidx')
        reshape long jobid, i(uuid `mthidx') j(`jobidx')
        generate byte wave = ceil(`mthidx'/4)
    }
    
    generate jobid_orig = jobid
    
    // import jobid revision file
    preserve
    
    __sipp_nber_import_jid, panel(`panel')
    save "`fn_jid'"

    restore
    
    merge m:1 uuid wave jobid using "`fn_jid'", noreport ///
        keepusing(jobid_revised flag_jobid_chan) 

    // do not use keep option so we explictly see how many obs. are in 'using'
    // only. These can only be due to errors.
    // Keep only master or matching obs.
    keep if _merge == 1 | _merge == 3
    drop _merge
    
    replace jobid = jobid_revised if flag_jobid_chan == 1
    
    if "`dataset'" == "core" {

       // restore previous format and variable names
        reshape wide jobid jobid_revised flag_jobid_chan jobid_orig, /// 
            i(uuid wave refmth) j(`jobidx')
        rename (jobid1 jobid2) (ws12002 ws22102)
    
        // FIXME: assert that the above gives results we expect -- should be commented out
        // in final version.
        assert ws12002 == jobid_orig1 if flag_jobid_chan1 != 1
        assert ws12002 == jobid_revised1 if flag_jobid_chan1 == 1
        assert ws22102 == jobid_orig2 if flag_jobid_chan2 != 1
        assert ws22102 == jobid_revised2 if flag_jobid_chan2 == 1
   
        // ID-type variables used to merge with original data set 
        local merge_vars uuid wave refmth
    } 
    else if "`dataset'" == "tm" {

        // reference wave for tm8214 differs for 1990/1991 and 1992/1993
        // panels!
        replace tm8214 = jobid if flag_jobid_chan == 1 & wave == `wave'
        if `panel' == 1990 | `panel' == 1991 {
            replace tm8206 = jobid if flag_jobid_chan == 1 & wave == 1
        }
        
        // FIXME: remove asserts in final version
        assert jobid_orig == tm8214 if flag_jobid_chan != 1 & wave == `wave'
        assert jobid_revised == tm8214 if flag_jobid_chan == 1 & wave == `wave'
        if `panel' == 1990 | `panel' == 1991 {
            assert jobid_orig == tm8206 if flag_jobid_chan != 1 & wave == 1
            assert jobid_revised == tm8206 if flag_jobid_chan == 1 & wave == 1
        }

        // jobid does not exist in tm files; wave variable will be merged from
        // original data set below
        drop jobid wave

        local merge_vars uuid
    }
    else if "`dataset'" == "fp" {
        
        drop wave
        reshape wide jobid jobid_revised flag_jobid_chan jobid_orig, ///
            i(uuid `mthidx') j(`jobidx')
    
        // FIXME: next line only needed for assets below, remove in final version
        levelsof `mthidx', local(mthval)
    
        reshape wide jobid1 jobid2 jobid_revised1 jobid_revised2 ///
            jobid_orig1 jobid_orig2 flag_jobid_chan1 flag_jobid_chan2, i(uuid) j(`mthidx')


        // FIXME: assert that results make sense -- should be removed in final version
        foreach m of local mthval {
            forvalues i = 1/2 {
                assert jobid`i'`m' == jobid_orig`i'`m' if flag_jobid_chan`i'`m' != 1
                assert jobid`i'`m' == jobid_revised`i'`m' if flag_jobid_chan`i'`m' == 1
            }
        }

        rename jobid?(#)            jobid?(##)
        rename jobid_orig?(#)       jobid_orig?(##)
        rename jobid_revised?(#)    jobid_revised?(##)
        rename flag_jobid_chan?(#)  flag_jobid_chan?(##)
    
        // ID-type variables used to merge with original data set 
        local merge_vars uuid
    }

    drop jobid_orig* jobid_revised*
    format %1.0f flag_jobid_chan*

    // for logging
    describe

    // revised job IDs are in master data set, thus these will be kept by
    // default, old version from using data set will be discarded
    merge 1:1 `merge_vars' using "`fn_orig'", assert(match) ///
        nogenerate nolabel nonotes

    // FIXME: debugging, remove in final version
    // Check that all the back-and-forth reshaping worked as expected.
    assert _N == `obs'

end // sipp_apply_rev

// ------------------------------------------------------------ 
// This program works analogously to the other sipp_nber_import_* programs.
// However, it is not meant to be called by users, as the job ID revision date
// is of no interest in its own right. Therefore we implement it in this
// revision file, as there is no need to call it from somewhere else.
capture program drop __sipp_nber_import_jid
program define __sipp_nber_import_jid

    syntax, Panel(integer)
    
    // store the dict, do and raw files names in r()
    quietly sipp_nber_file_names, panel(`panel') dataset("jid")
    
    quietly infile using "${nber_dict_dir}/`r(dct)'", using("${nber_raw_dir}/`r(raw)'") clear
    run "${nber_do_dir}/`r(lbl)'"

    sipp_gen_id, dataset("jid") generate(uuid)

end // sipp_nber_import_jid


// vim: set sw=4 sts=4 et:
