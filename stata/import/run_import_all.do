
clear *

set maxvar 10000

run "sipp_import_env.do"

set more off
set trace off
pause off

capture log close

log using "${log_dir}/sipp-import-all.log", replace

foreach pid of numlist 1990/1993 1996 2001 2004 2008 {
    sipp_nber_import_core, panel(`pid')
    sipp_nber_import_tm, panel(`pid')
    sipp_nber_import_fp, panel(`pid')
    sipp_nber_import_wgt, panel(`pid')
}

/*
foreach pid of numlist 1990/1993 {
    sipp_nber_import_fp, panel(`pid')
}
*/


log close
