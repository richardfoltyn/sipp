// Program to import topical module data from NBER raw files to Stata dta format.
// Author: Richard Foltyn
// FIXME: licensing

capture program drop sipp_nber_import_tm
program define sipp_nber_import_tm

// technically a particular topical module does not consist of several waves but
// instead a different module is surveyed at each wave. We continue to refer to
// this topical module number as 'wave'.
syntax, Panel(integer) [WAVes(numlist integer sort)]

// assert whether this is a valid panel id
dbg_off sipp_assert_panel `panel'

// if no waves argument present, check what we have on disk and use
// that instead. Map found files to wave indices, these are needed
// below.
if `"`waves'"' == "" {
    dbg_off sipp_nber_find_waves, panel(`panel') dictdir("${nber_dict_dir}") dataset("tm")
    local waves = r(waves)
}

foreach i of local waves  {

    // need to drop label definitions from previous waves
    dbg_off clear

    // store the dict, do and raw files names in r()
    dbg_off sipp_nber_file_names, panel(`panel') dataset("tm") wave(`i')

    quietly infile using "${nber_dict_dir}/`r(dct)'", using("${nber_raw_dir}/`r(raw)'")
    dbg_off run "${nber_do_dir}/`r(lbl)'"

    sipp_gen_id, panel(`panel') generate(uuid) dataset("tm")

    // apply data revision for which no updated core files have been
    // published. See SIPP user notes for details.
    sipp_apply_revisions, panel(`panel') wave(`i') dataset("tm")

    run "sipp_custom_formats.do"
    run "sipp_custom_labels.do"

    sort uuid 

    // retrieve 'canonical' file name for output file
    dbg_off sipp_gen_fname, panel(`panel') wave(`i') dataset("tm")
    save "${nber_out_dir}/`r(fname)'", replace
}

end // sipp_nber_import_tm


