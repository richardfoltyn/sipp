Stata dictionary files for raw data
==========================================

Introduction
------------

This directory contains the modified Stata dictionaries from [NBER's SIPP site] [nber] 
used to import the raw SIPP data.
 
 * These files are automatically generated using scripts in the _/tools_
      directory. Do not edit manually.
 * For copyright and licensing, see the comment at the beginning of each file.

[nber]: http://www.nber.org/data/sipp.html

Changes applied to original dictionary files
--------------------------------------------

 *  Drop hardcoded raw data file name, this is too unflexible
 *  Changes data types from `strXX` to numeric types
      `byte`/`int`/`long`/`double`, depending on number of digits.
    This in particular applies to the variables
    *   suseqnum
    *   ssuid, suid, id, pp\_id
    *   addid, eentaid
    *   entry, pp\_entry 
    *   pnum, epppnum, pp\_pnum
 *  For 1992 full panel files, rename pp\_int to pp\_intv to ensure consistent
    naming across 1990-1993 full panel files.
 *  Uncommented all field names that are commented out int NBER files to respect
    built-in limit on maximum number of variables in some Stata versions.
 *  Move copyright comment inside the dictionary declaration (according to Stata
    documentation this is the only valid location)
 *  Delete all comments except for copyright notice.

