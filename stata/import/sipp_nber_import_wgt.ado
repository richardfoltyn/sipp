// Imports longitudinal weight raw data files from NBER server into Stata
// format.

// Author: Richard Foltyn
// FIXME: licensing

capture program drop sipp_nber_import_wgt
program define sipp_nber_import_wgt

syntax, Panel(integer)

dbg_off sipp_assert_panel `panel'

if `panel' < 1996 {
    display as error "Invalid panel. Longitudinal weights available only for 1996, 2001, 2004 and 2008."
    exit
}

// clear explicitly to remove possibly remaining labels from memory
dbg_off clear

dbg_off sipp_nber_file_names, panel(`panel') dataset("wgt")
quietly infile using "${nber_dict_dir}/`r(dct)'", using("${nber_raw_dir}/`r(raw)'")
dbg_off run "${nber_do_dir}/`r(lbl)'"

dbg_off sipp_gen_id, dataset("wgt") generate("uuid")

run "sipp_custom_formats.do"
run "sipp_custom_labels.do"

sort uuid

dbg_off sipp_gen_fname, panel(`panel') dataset("wgt") 
save "${nber_out_dir}/`r(fname)'", replace

end // sipp_nber_import_wgt

// vim: sts=4 sw=4 et:
