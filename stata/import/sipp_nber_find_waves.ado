// Program to detect which wave files  are present in some local directory.
// Files are matched according to the naming scheme of the NBER SIPP site.
// Author: Richard Foltyn
// FIXME: Licensing

capture program drop sipp_nber_find_waves
program define sipp_nber_find_waves, rclass

    syntax, Panel(integer) DICTDIR(string) [DATaset(string)]

    if `"`dataset'"' == "" {
        local dataset = "core"
    }
    
    // obtain pattern to search for
    quietly sipp_nber_file_names, panel(`panel') dataset(`dataset')

    local foundfiles: dir "`dictdir'" files "`r(dct)'"

    foreach f of local foundfiles {
        local status = regexm("`f'", ".*[^0-9]+([0-9]+)\.dct$")
        // ensure that regex actually matches
        if `status' == 1 {
            local waves = "`waves' " + regexs(1)
        }
    }

    // infortunately this is only sorted alphabetically, i.e. we get 1 10 11 12 2
    // ... for panels with > 10 waves
    local waves: list sort waves
    local nwaves: word count `waves'

    return local waves = "`waves'"
    return scalar nwaves = `nwaves'
end // sipp_nber_find_waves

// vim: set sts=4 sw=4 et:
