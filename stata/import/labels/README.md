Stata do-files to apply labels to raw data
==========================================

Introcution
-----------

This directory contains the Stata do-files from [NBER's SIPP site] [nber] which
define the value labels for raw data files.

 * These files are automatically generated using scripts in the `/tools`
      directory. Do not edit manually.
 * For copyright and licensing, see the comment at the beginning of each file.

[nber]: http://www.nber.org/data/sipp.html

Changes applied to NBER do-files
--------------------------------

 *  For 1992 panel, rename variables / label names from `pp_int` to `pp_intv` to
    be consistent with 1990-1993 panels.
 *  Uncomment labels commented out in NBER files which refer to variables
    dropped from dictionary files in order to respect limit on number of
    variables of some Stata versions.
 *  Remove any Stata commands expect for label definitions
 *  Remove all comments except for copyright notice.
 *  Move copyright notice to beginning of file.
