// Program that maps panel year, data set type (core, topical, etc.) and wave
// index to the corresponding file name as used on the NBER SIPP web site.
// These file names are highly inconsistent across panels, therefore we
// bundle the logic in a separate program.

// Author: Richard Foltyn
// FIXME: licensing

capture program drop sipp_nber_file_names
capture program define sipp_nber_file_names, rclass

    syntax, Panel(integer) Dataset(string) [Wave(integer 0)]

    sipp_assert_dataset `dataset'

    // filenames use only last 2 digits to identify panel
    local pid = substr("`panel'", 3, 2)
   
    // For core, topical, job ID and full panel data, filenams loosly conform
    // to naming scheme pre1 + pre2 + YY + suf1 + suf2 + DD + ".ext"
    // Naming convention for raw files again differs.
    // default prefixes and suffixes - all others can be left undefined.
    local pre1 = "sip"
    local pre1r = "sipp"

    // adjust for deviations from default naming scheme
    if "`dataset'" == "core" {
        local pre1 = cond(`panel' == 2008, "sipp", "sip")
        local pre2 = cond(`panel' == 2008, "l", "")
        local suf1 = cond(`panel' == 2008, "pu", "")
        local suf2 = cond(`panel' == 1996, "l", "w")

        local pre1r = cond(`panel' == 2004 | `panel' == 2008, "", "sipp")
        local pre2r = cond(`panel' == 2004 | `panel' == 2008, "l", "")
        local suf1r = cond(`panel' == 2004 | `panel' == 2008, "pu", "")
        local suf2r = "`suf2'"
    }
    else if "`dataset'" == "tm" {
        local pre1 = cond(`panel' == 2008, "sipp", "sip")
        local pre2 = cond(`panel' == 2008, "p", "")
        local suf1 = cond(`panel' == 2008, "pu", "")
        local suf2 = cond(`panel' == 2008, "tm", "t")

        local pre1r = cond(`panel' == 2004 | `panel' == 2008, "", "sipp")
        local pre2r = cond(`panel' == 2004 | `panel' == 2008, "p", "")
        local suf1r = cond(`panel' == 2004 | `panel' == 2008, "pu", "")
        local suf2r = cond(`panel' == 2004 | `panel' == 2008, "tm", "t")
    }
    else if "`dataset'" == "jid" {
        local suf2 = "jid"
        local suf2r = "jid"
    }
    else if "`dataset'" == "fp" {
        local suf2 = "fp"
        local suf2r = "fp"
    }

    // place holder for wave digits. Applicable only to core and topical data sets
    local ww = cond(`"`dataset'"' == "core" | `"`dataset'"' == "tm", "*", "")

    if `"`dataset'"' == "core" | `"`dataset'"' == "tm" | ///
        `"`dataset'"' == "jid" | `"`dataset'"' == "fp" {

        local fdict = "`pre1'`pre2'`pid'`suf1'`suf2'`ww'.dct"
        local fraw = "`pre1r'`pre2r'`pid'`suf1r'`suf2r'`ww'.dat"
        local flabel = "`pre1'`pre2'`pid'`suf1'`suf2'`ww'.do"
    }

    // file names for longitudinal weights are the most incosistent, there
    // is not much benefit in trying to squeeze them into a generic naming
    // scheme. We just hardcode all file names.
    if "`dataset'" == "wgt" {
        if `panel' == 1996 {
            local fdict = "sip96lw.dct"
            local flabel = "sip96lw.do"
            local fraw = "sipp96lw.dat"
        }
        else if `panel' == 2001 {
            local fdict = "sip01lw9.dct"
            local flabel = "sip01lw9.do"
            local fraw = "sipp01lw9.dat"
        }
        else if `panel' == 2004 {
            local fdict = "sip04lw4.dct"
            local flabel = "sip04lw4.do"
            local fraw = "lgtwgt2004w12.dat"
        }
        else if `panel' == 2008 {
            // TODO: these filenames are correct as of 2013-07-12, but will
            // most likely change in future as additional weight data is
            // released. 
            local fdict = "sipplgtwgt2008w7.dct"
            local flabel = "sipplgtwgt2008w7.do"
            local fraw = "lgtwgt2008w7.dat"
        }
        else {
            display as error "No longitudinal weight files available for panel `panel'!"
            exit
        }
    }

    // if user specified particular index, use it
    if `wave' > 0 {
        foreach lm in "fdict" "fraw" "flabel" {
            local `lm' = subinstr("``lm''", "*", "`wave'", 1)
        }
    }


    return local dct = "`fdict'"
    return local raw = "`fraw'"
    return local lbl = "`flabel'"

end

// ------------------------------------------------------------------------- 
// Unit test for sipp_nber_file_names: compare whether the names we generate
// correspond to what's on disk.
// Not meant to be run by end users, for debugging purposes only!
capture program drop __sipp_nber_file_names_test
program define __sipp_nber_file_names_test

    // pass base directory which contains do/, dict/ and raw/ subdirectories
    args basedir
    
    // hardcode what waves we'd expect to find for topical and core modules.
    local tm_1990 2/8
    local tm_1991 2/8
    local tm_1992 1/9
    local tm_1993 1/9
    local tm_1996 1/12
    local tm_2001 1/9
    local tm_2004 1/8
    local tm_2008 1/11

    // number of core module waves
    local core_1990 1/8
    local core_1991 1/8
    // note that the number of waves in the User guide is wrong, there seem
    // to be only 9 for the 1992 panel
    local core_1992 1/9
    local core_1993 1/9
    local core_1996 1/12
    local core_2001 1/9
    local core_2004 1/12
    local core_2008 1/12


    foreach pid of numlist 1990 1991 1992 1993 1996 2001 2004 2008 {
        foreach ds in "core" "tm" {

            forvalues w = ``ds'_`pid'' {
                quietly sipp_nber_file_names, panel(`pid') dataset(`ds') ///
                    wave(`w')

                disp "Checking `pid' wave `w' (`ds')"

                confirm file "`basedir'/dict/`r(dct)'"
                confirm file "`basedir'/do/`r(lbl)'"
                confirm file "`basedir'/raw/`r(raw)'"
            }
        }
    }

    forvalues pid  = 1990/1993 {
        quietly sipp_nber_file_names, panel(`pid') dataset("fp")

        display "Checking full panel files for `pid'"
        confirm file "`basedir'/dict/`r(dct)'"
        confirm file "`basedir'/do/`r(lbl)'"
        confirm file "`basedir'/raw/`r(raw)'"
    }

    foreach pid of numlist 1996 2001 2004 2008 {
        quietly sipp_nber_file_names, panel(`pid') dataset("wgt")

        display "Checking longitudinal weight files for `pid'"
        confirm file "`basedir'/dict/`r(dct)'"
        confirm file "`basedir'/do/`r(lbl)'"
        confirm file "`basedir'/raw/`r(raw)'"
    }

end
