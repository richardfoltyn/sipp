// Apply custom variable and value labels that override those from
// automatically generated do and dictionary files.

// Author: Richard Foltyn
// FIXME: licensing

// value labels for reference months and rotation groups are not exactly
// helpful.
capture label drop srefmon
capture label drop rot

// vim: set sts=4 sw=4 et::w
