// Applies formats to some important variables.
// Should be applied after importing raw SIPP data using NBER do-files.

// Author: Richard Foltyn
// FIXME: licensing


// 1996+ core wave files
capture format %012.0f ssuid
capture format %03.0f eentaid
capture format %04.0f epppnum
capture format %4.0f spanel
capture format %2.0f swave
capture format %1.0f srefmon srotation
capture format %2.0f rhcalmn
capture format %4.0f rhcalyr

// pre-1996 core wave files
capture format %09.0f suid
capture format %02.0f entry
capture format %03.0f pnum
capture format %1.0f wave refmth
capture format %2.0f panel
capture format %1.0f rot

// longitudinal files
capture format %09.0f pp_id
// pp_enty and pp_pnum for 1992 full panel have 1 more digit that remaining
// pre-1996 full panel files
capture format %03.0f pp_entry
capture format %04.0f pp_pnum

// topical module files, pre-1996
capture format %09.0f id
