// Sets various panel-specific metadata such as number of waves, indices of topical modules, etc.
// This is unnecessarily tedious, but Stata supports no arrays... :(

capture program drop sipp_panelmd
program define sipp_panelmd

// indices of topical module files for each panel
// need to be entered as syntax compliant with numlist
global tm_idx_1990 "2/8"
global tm_idx_1991 "2/8"
global tm_idx_1992 "1/9"
global tm_idx_1993 "1/9"
global tm_idx_1996 "1/12"
global tm_idx_2001 "1/9"
global tm_idx_2004 "1/8"
global tm_idx_2008 "1/11"

// number of core module waves
global n_wav_1990 8
global n_wav_1991 8
// note that the number of waves in the User guide is wrong, there seem
// to be only 9 for the 1992 panel
global n_wav_1992 9
global n_wav_1993 9
global n_wav_1996 12
global n_wav_2001 9
global n_wav_2004 12
global n_wav_2008 12

// for the CEPR recoding we need the start and end years for the reference months of each panel
global syear_1990 = 1989
global eyear_1990 = 1992

global syear_1991 = 1990
global eyear_1991 = 1993

global syear_1992 = 1991
global eyear_1992 = 1995

global syear_1993 = 1992
global eyear_1993 = 1996

global syear_1996 = 1996
global eyear_1996 = 202000

global syear_2001 = 2000
global eyear_2001 = 2004

global syear_2004 = 2003
global eyear_2004 = 2008

global syear_2008 = 2008
global eyear_2008 = 2012

end
